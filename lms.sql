toc.dat                                                                                             0000600 0004000 0002000 00000073727 12574536427 014476  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP       9                    s            lms    9.4.4    9.4.4 p    a           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false         b           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false         c           1262    49432    lms    DATABASE     �   CREATE DATABASE lms WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE lms;
             postgres    false                     2615    49548    profile    SCHEMA        CREATE SCHEMA profile;
    DROP SCHEMA profile;
             postgres    false                     2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false         d           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5         e           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    5         �            3079    11855    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false         f           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    196         �            1259    49549    author    TABLE     i   CREATE TABLE author (
    author_id integer NOT NULL,
    author_name character varying(255) NOT NULL
);
    DROP TABLE profile.author;
       profile         postgres    false    7         �            1259    49552    author_author_id_seq    SEQUENCE     v   CREATE SEQUENCE author_author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE profile.author_author_id_seq;
       profile       postgres    false    7    173         g           0    0    author_author_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE author_author_id_seq OWNED BY author.author_id;
            profile       postgres    false    174         �            1259    98620    barcode    TABLE     X   CREATE TABLE barcode (
    barcode_id integer NOT NULL,
    book_id integer NOT NULL
);
    DROP TABLE profile.barcode;
       profile         postgres    false    7         �            1259    49554    book    TABLE     �  CREATE TABLE book (
    book_id integer NOT NULL,
    book_name character varying(255) NOT NULL,
    book_edition integer NOT NULL,
    author_id integer,
    book_number character varying(255),
    publisher character varying(255),
    quantity integer,
    book_status character varying(255),
    borrowed_book_count integer,
    reserve_book_count integer,
    barcode_id character varying(255)
);
    DROP TABLE profile.book;
       profile         postgres    false    7         �            1259    49560    book_book_id_seq    SEQUENCE     r   CREATE SEQUENCE book_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE profile.book_book_id_seq;
       profile       postgres    false    175    7         h           0    0    book_book_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE book_book_id_seq OWNED BY book.book_id;
            profile       postgres    false    176         �            1259    49562    borrow_book    TABLE     �   CREATE TABLE borrow_book (
    borrow_book_id integer NOT NULL,
    borrow_date date NOT NULL,
    expiry_date date NOT NULL,
    book_id integer NOT NULL,
    user_id integer
);
     DROP TABLE profile.borrow_book;
       profile         postgres    false    7         �            1259    49565    borrow_book_borrow_book_id_seq    SEQUENCE     �   CREATE SEQUENCE borrow_book_borrow_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE profile.borrow_book_borrow_book_id_seq;
       profile       postgres    false    177    7         i           0    0    borrow_book_borrow_book_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE borrow_book_borrow_book_id_seq OWNED BY borrow_book.borrow_book_id;
            profile       postgres    false    178         �            1259    57394    feedback    TABLE     �   CREATE TABLE feedback (
    feedback_id integer NOT NULL,
    user_id integer,
    date date,
    description character varying(255),
    subject character varying(255)
);
    DROP TABLE profile.feedback;
       profile         postgres    false    7         �            1259    57397    feedback_feedback_id_seq    SEQUENCE     z   CREATE SEQUENCE feedback_feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE profile.feedback_feedback_id_seq;
       profile       postgres    false    187    7         j           0    0    feedback_feedback_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE feedback_feedback_id_seq OWNED BY feedback.feedback_id;
            profile       postgres    false    188         �            1259    90349    fines    TABLE     w   CREATE TABLE fines (
    fine_id integer NOT NULL,
    book_id integer,
    user_id integer,
    fineamount integer
);
    DROP TABLE profile.fines;
       profile         postgres    false    7         �            1259    90352    fines_fine_id_seq    SEQUENCE     s   CREATE SEQUENCE fines_fine_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE profile.fines_fine_id_seq;
       profile       postgres    false    7    193         k           0    0    fines_fine_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE fines_fine_id_seq OWNED BY fines.fine_id;
            profile       postgres    false    194         �            1259    49567    groups    TABLE     f   CREATE TABLE groups (
    group_id integer NOT NULL,
    groupname character varying(255) NOT NULL
);
    DROP TABLE profile.groups;
       profile         postgres    false    7         �            1259    49570    group_group_id_seq    SEQUENCE     t   CREATE SEQUENCE group_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE profile.group_group_id_seq;
       profile       postgres    false    179    7         l           0    0    group_group_id_seq    SEQUENCE OWNED BY     <   ALTER SEQUENCE group_group_id_seq OWNED BY groups.group_id;
            profile       postgres    false    180         �            1259    57411    request_new_book    TABLE     �   CREATE TABLE request_new_book (
    request_new_book_id integer NOT NULL,
    book_name character varying(255),
    author character varying(255),
    publisher character varying(255),
    edition integer,
    user_id integer
);
 %   DROP TABLE profile.request_new_book;
       profile         postgres    false    7         �            1259    57414 (   request_new_book_request_new_book_id_seq    SEQUENCE     �   CREATE SEQUENCE request_new_book_request_new_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE profile.request_new_book_request_new_book_id_seq;
       profile       postgres    false    7    189         m           0    0 (   request_new_book_request_new_book_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE request_new_book_request_new_book_id_seq OWNED BY request_new_book.request_new_book_id;
            profile       postgres    false    190         �            1259    49585    reserve_book    TABLE     �   CREATE TABLE reserve_book (
    reserve_book_id integer NOT NULL,
    reserve_start_date timestamp without time zone DEFAULT now() NOT NULL,
    book_id integer NOT NULL,
    reserve_finish_date date,
    user_id integer
);
 !   DROP TABLE profile.reserve_book;
       profile         postgres    false    7         �            1259    49588     reserve_book_reserve_book_id_seq    SEQUENCE     �   CREATE SEQUENCE reserve_book_reserve_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE profile.reserve_book_reserve_book_id_seq;
       profile       postgres    false    181    7         n           0    0     reserve_book_reserve_book_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE reserve_book_reserve_book_id_seq OWNED BY reserve_book.reserve_book_id;
            profile       postgres    false    182         �            1259    49590    return_book    TABLE     �   CREATE TABLE return_book (
    return_book_id integer NOT NULL,
    return_date date NOT NULL,
    book_id integer NOT NULL,
    user_id integer
);
     DROP TABLE profile.return_book;
       profile         postgres    false    7         �            1259    49593    return_book_return_book_id_seq    SEQUENCE     �   CREATE SEQUENCE return_book_return_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE profile.return_book_return_book_id_seq;
       profile       postgres    false    183    7         o           0    0    return_book_return_book_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE return_book_return_book_id_seq OWNED BY return_book.return_book_id;
            profile       postgres    false    184         �            1259    49603    user    TABLE     �  CREATE TABLE "user" (
    user_id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    contact character varying,
    email character varying,
    address character varying(255),
    image bytea,
    name character varying(255),
    status boolean,
    theme character varying(255),
    group_id integer,
    barcode_id integer
);
    DROP TABLE profile."user";
       profile         postgres    false    7         �            1259    65646    user_history    TABLE     �   CREATE TABLE user_history (
    user_history_id integer NOT NULL,
    username character varying,
    time_stamp date DEFAULT now()
);
 !   DROP TABLE profile.user_history;
       profile         postgres    false    7         �            1259    65644     user_history_user_history_id_seq    SEQUENCE     �   CREATE SEQUENCE user_history_user_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE profile.user_history_user_history_id_seq;
       profile       postgres    false    7    192         p           0    0     user_history_user_history_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE user_history_user_history_id_seq OWNED BY user_history.user_history_id;
            profile       postgres    false    191         �            1259    49609    user_user_id_seq    SEQUENCE     r   CREATE SEQUENCE user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE profile.user_user_id_seq;
       profile       postgres    false    185    7         q           0    0    user_user_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE user_user_id_seq OWNED BY "user".user_id;
            profile       postgres    false    186         �           2604    49611 	   author_id    DEFAULT     f   ALTER TABLE ONLY author ALTER COLUMN author_id SET DEFAULT nextval('author_author_id_seq'::regclass);
 @   ALTER TABLE profile.author ALTER COLUMN author_id DROP DEFAULT;
       profile       postgres    false    174    173         �           2604    49612    book_id    DEFAULT     ^   ALTER TABLE ONLY book ALTER COLUMN book_id SET DEFAULT nextval('book_book_id_seq'::regclass);
 <   ALTER TABLE profile.book ALTER COLUMN book_id DROP DEFAULT;
       profile       postgres    false    176    175         �           2604    49613    borrow_book_id    DEFAULT     z   ALTER TABLE ONLY borrow_book ALTER COLUMN borrow_book_id SET DEFAULT nextval('borrow_book_borrow_book_id_seq'::regclass);
 J   ALTER TABLE profile.borrow_book ALTER COLUMN borrow_book_id DROP DEFAULT;
       profile       postgres    false    178    177         �           2604    57399    feedback_id    DEFAULT     n   ALTER TABLE ONLY feedback ALTER COLUMN feedback_id SET DEFAULT nextval('feedback_feedback_id_seq'::regclass);
 D   ALTER TABLE profile.feedback ALTER COLUMN feedback_id DROP DEFAULT;
       profile       postgres    false    188    187         �           2604    90354    fine_id    DEFAULT     `   ALTER TABLE ONLY fines ALTER COLUMN fine_id SET DEFAULT nextval('fines_fine_id_seq'::regclass);
 =   ALTER TABLE profile.fines ALTER COLUMN fine_id DROP DEFAULT;
       profile       postgres    false    194    193         �           2604    49614    group_id    DEFAULT     c   ALTER TABLE ONLY groups ALTER COLUMN group_id SET DEFAULT nextval('group_group_id_seq'::regclass);
 ?   ALTER TABLE profile.groups ALTER COLUMN group_id DROP DEFAULT;
       profile       postgres    false    180    179         �           2604    57416    request_new_book_id    DEFAULT     �   ALTER TABLE ONLY request_new_book ALTER COLUMN request_new_book_id SET DEFAULT nextval('request_new_book_request_new_book_id_seq'::regclass);
 T   ALTER TABLE profile.request_new_book ALTER COLUMN request_new_book_id DROP DEFAULT;
       profile       postgres    false    190    189         �           2604    49617    reserve_book_id    DEFAULT     ~   ALTER TABLE ONLY reserve_book ALTER COLUMN reserve_book_id SET DEFAULT nextval('reserve_book_reserve_book_id_seq'::regclass);
 L   ALTER TABLE profile.reserve_book ALTER COLUMN reserve_book_id DROP DEFAULT;
       profile       postgres    false    182    181         �           2604    49618    return_book_id    DEFAULT     z   ALTER TABLE ONLY return_book ALTER COLUMN return_book_id SET DEFAULT nextval('return_book_return_book_id_seq'::regclass);
 J   ALTER TABLE profile.return_book ALTER COLUMN return_book_id DROP DEFAULT;
       profile       postgres    false    184    183         �           2604    49620    user_id    DEFAULT     `   ALTER TABLE ONLY "user" ALTER COLUMN user_id SET DEFAULT nextval('user_user_id_seq'::regclass);
 >   ALTER TABLE profile."user" ALTER COLUMN user_id DROP DEFAULT;
       profile       postgres    false    186    185         �           2604    65649    user_history_id    DEFAULT     ~   ALTER TABLE ONLY user_history ALTER COLUMN user_history_id SET DEFAULT nextval('user_history_user_history_id_seq'::regclass);
 L   ALTER TABLE profile.user_history ALTER COLUMN user_history_id DROP DEFAULT;
       profile       postgres    false    191    192    192         H          0    49549    author 
   TABLE DATA               1   COPY author (author_id, author_name) FROM stdin;
    profile       postgres    false    173       2120.dat r           0    0    author_author_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('author_author_id_seq', 7, true);
            profile       postgres    false    174         ^          0    98620    barcode 
   TABLE DATA               /   COPY barcode (barcode_id, book_id) FROM stdin;
    profile       postgres    false    195       2142.dat J          0    49554    book 
   TABLE DATA               �   COPY book (book_id, book_name, book_edition, author_id, book_number, publisher, quantity, book_status, borrowed_book_count, reserve_book_count, barcode_id) FROM stdin;
    profile       postgres    false    175       2122.dat s           0    0    book_book_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('book_book_id_seq', 12, true);
            profile       postgres    false    176         L          0    49562    borrow_book 
   TABLE DATA               Z   COPY borrow_book (borrow_book_id, borrow_date, expiry_date, book_id, user_id) FROM stdin;
    profile       postgres    false    177       2124.dat t           0    0    borrow_book_borrow_book_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('borrow_book_borrow_book_id_seq', 5, true);
            profile       postgres    false    178         V          0    57394    feedback 
   TABLE DATA               M   COPY feedback (feedback_id, user_id, date, description, subject) FROM stdin;
    profile       postgres    false    187       2134.dat u           0    0    feedback_feedback_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('feedback_feedback_id_seq', 6, true);
            profile       postgres    false    188         \          0    90349    fines 
   TABLE DATA               ?   COPY fines (fine_id, book_id, user_id, fineamount) FROM stdin;
    profile       postgres    false    193       2140.dat v           0    0    fines_fine_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('fines_fine_id_seq', 3, true);
            profile       postgres    false    194         w           0    0    group_group_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('group_group_id_seq', 2, true);
            profile       postgres    false    180         N          0    49567    groups 
   TABLE DATA               .   COPY groups (group_id, groupname) FROM stdin;
    profile       postgres    false    179       2126.dat X          0    57411    request_new_book 
   TABLE DATA               h   COPY request_new_book (request_new_book_id, book_name, author, publisher, edition, user_id) FROM stdin;
    profile       postgres    false    189       2136.dat x           0    0 (   request_new_book_request_new_book_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('request_new_book_request_new_book_id_seq', 1, false);
            profile       postgres    false    190         P          0    49585    reserve_book 
   TABLE DATA               k   COPY reserve_book (reserve_book_id, reserve_start_date, book_id, reserve_finish_date, user_id) FROM stdin;
    profile       postgres    false    181       2128.dat y           0    0     reserve_book_reserve_book_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('reserve_book_reserve_book_id_seq', 81, true);
            profile       postgres    false    182         R          0    49590    return_book 
   TABLE DATA               M   COPY return_book (return_book_id, return_date, book_id, user_id) FROM stdin;
    profile       postgres    false    183       2130.dat z           0    0    return_book_return_book_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('return_book_return_book_id_seq', 1, false);
            profile       postgres    false    184         T          0    49603    user 
   TABLE DATA               �   COPY "user" (user_id, username, password, contact, email, address, image, name, status, theme, group_id, barcode_id) FROM stdin;
    profile       postgres    false    185       2132.dat [          0    65646    user_history 
   TABLE DATA               F   COPY user_history (user_history_id, username, time_stamp) FROM stdin;
    profile       postgres    false    192       2139.dat {           0    0     user_history_user_history_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('user_history_user_history_id_seq', 857, true);
            profile       postgres    false    191         |           0    0    user_user_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('user_user_id_seq', 18, true);
            profile       postgres    false    186         �           2606    49622 	   author_id 
   CONSTRAINT     N   ALTER TABLE ONLY author
    ADD CONSTRAINT author_id PRIMARY KEY (author_id);
 ;   ALTER TABLE ONLY profile.author DROP CONSTRAINT author_id;
       profile         postgres    false    173    173         �           2606    98629 
   barcode_id 
   CONSTRAINT     Q   ALTER TABLE ONLY barcode
    ADD CONSTRAINT barcode_id PRIMARY KEY (barcode_id);
 =   ALTER TABLE ONLY profile.barcode DROP CONSTRAINT barcode_id;
       profile         postgres    false    195    195         �           2606    49624    book_id 
   CONSTRAINT     H   ALTER TABLE ONLY book
    ADD CONSTRAINT book_id PRIMARY KEY (book_id);
 7   ALTER TABLE ONLY profile.book DROP CONSTRAINT book_id;
       profile         postgres    false    175    175         �           2606    49626    borrow_book_id 
   CONSTRAINT     ]   ALTER TABLE ONLY borrow_book
    ADD CONSTRAINT borrow_book_id PRIMARY KEY (borrow_book_id);
 E   ALTER TABLE ONLY profile.borrow_book DROP CONSTRAINT borrow_book_id;
       profile         postgres    false    177    177         �           2606    57404    feedback_id 
   CONSTRAINT     T   ALTER TABLE ONLY feedback
    ADD CONSTRAINT feedback_id PRIMARY KEY (feedback_id);
 ?   ALTER TABLE ONLY profile.feedback DROP CONSTRAINT feedback_id;
       profile         postgres    false    187    187         �           2606    90359    fine_id 
   CONSTRAINT     I   ALTER TABLE ONLY fines
    ADD CONSTRAINT fine_id PRIMARY KEY (fine_id);
 8   ALTER TABLE ONLY profile.fines DROP CONSTRAINT fine_id;
       profile         postgres    false    193    193         �           2606    49628    group_id 
   CONSTRAINT     L   ALTER TABLE ONLY groups
    ADD CONSTRAINT group_id PRIMARY KEY (group_id);
 :   ALTER TABLE ONLY profile.groups DROP CONSTRAINT group_id;
       profile         postgres    false    179    179         �           2606    57424    request_new_book_id 
   CONSTRAINT     l   ALTER TABLE ONLY request_new_book
    ADD CONSTRAINT request_new_book_id PRIMARY KEY (request_new_book_id);
 O   ALTER TABLE ONLY profile.request_new_book DROP CONSTRAINT request_new_book_id;
       profile         postgres    false    189    189         �           2606    49634    reserve_book_id 
   CONSTRAINT     `   ALTER TABLE ONLY reserve_book
    ADD CONSTRAINT reserve_book_id PRIMARY KEY (reserve_book_id);
 G   ALTER TABLE ONLY profile.reserve_book DROP CONSTRAINT reserve_book_id;
       profile         postgres    false    181    181         �           2606    49636    return_book_id 
   CONSTRAINT     ]   ALTER TABLE ONLY return_book
    ADD CONSTRAINT return_book_id PRIMARY KEY (return_book_id);
 E   ALTER TABLE ONLY profile.return_book DROP CONSTRAINT return_book_id;
       profile         postgres    false    183    183         �           2606    65655    user_history_id 
   CONSTRAINT     `   ALTER TABLE ONLY user_history
    ADD CONSTRAINT user_history_id PRIMARY KEY (user_history_id);
 G   ALTER TABLE ONLY profile.user_history DROP CONSTRAINT user_history_id;
       profile         postgres    false    192    192         �           2606    49640    user_id 
   CONSTRAINT     J   ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_id PRIMARY KEY (user_id);
 9   ALTER TABLE ONLY profile."user" DROP CONSTRAINT user_id;
       profile         postgres    false    185    185         �           1259    57440 
   fk_user_id    INDEX     C   CREATE INDEX fk_user_id ON request_new_book USING btree (user_id);
    DROP INDEX profile.fk_user_id;
       profile         postgres    false    189         �           1259    49642    fki_author_id    INDEX     <   CREATE INDEX fki_author_id ON book USING btree (author_id);
 "   DROP INDEX profile.fki_author_id;
       profile         postgres    false    175         �           1259    98574    fki_book_id    INDEX     ?   CREATE INDEX fki_book_id ON borrow_book USING btree (book_id);
     DROP INDEX profile.fki_book_id;
       profile         postgres    false    177         �           1259    98585    fki_group_id    INDEX     <   CREATE INDEX fki_group_id ON "user" USING btree (group_id);
 !   DROP INDEX profile.fki_group_id;
       profile         postgres    false    185         �           1259    57410    fki_user_id    INDEX     <   CREATE INDEX fki_user_id ON feedback USING btree (user_id);
     DROP INDEX profile.fki_user_id;
       profile         postgres    false    187         �           1259    98596    fkia_book_id    INDEX     A   CREATE INDEX fkia_book_id ON reserve_book USING btree (book_id);
 !   DROP INDEX profile.fkia_book_id;
       profile         postgres    false    181         �           1259    65609    fkibb_user_id    INDEX     A   CREATE INDEX fkibb_user_id ON borrow_book USING btree (user_id);
 "   DROP INDEX profile.fkibb_user_id;
       profile         postgres    false    177         �           1259    65632    fkir_user_id    INDEX     A   CREATE INDEX fkir_user_id ON reserve_book USING btree (user_id);
 !   DROP INDEX profile.fkir_user_id;
       profile         postgres    false    181         �           1259    65643    fkire_user_id    INDEX     A   CREATE INDEX fkire_user_id ON return_book USING btree (user_id);
 "   DROP INDEX profile.fkire_user_id;
       profile         postgres    false    183         �           1259    98602    fkis_book_id    INDEX     @   CREATE INDEX fkis_book_id ON return_book USING btree (book_id);
 !   DROP INDEX profile.fkis_book_id;
       profile         postgres    false    183         �           2606    49643 	   author_id    FK CONSTRAINT     i   ALTER TABLE ONLY book
    ADD CONSTRAINT author_id FOREIGN KEY (author_id) REFERENCES author(author_id);
 9   ALTER TABLE ONLY profile.book DROP CONSTRAINT author_id;
       profile       postgres    false    1965    175    173         �           2606    90360    book_id    FK CONSTRAINT     b   ALTER TABLE ONLY fines
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);
 8   ALTER TABLE ONLY profile.fines DROP CONSTRAINT book_id;
       profile       postgres    false    175    1967    193         �           2606    98569    book_id    FK CONSTRAINT     h   ALTER TABLE ONLY borrow_book
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);
 >   ALTER TABLE ONLY profile.borrow_book DROP CONSTRAINT book_id;
       profile       postgres    false    175    1967    177         �           2606    98597    book_id    FK CONSTRAINT     h   ALTER TABLE ONLY return_book
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);
 >   ALTER TABLE ONLY profile.return_book DROP CONSTRAINT book_id;
       profile       postgres    false    183    175    1967         �           2606    98608    book_id    FK CONSTRAINT     i   ALTER TABLE ONLY reserve_book
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);
 ?   ALTER TABLE ONLY profile.reserve_book DROP CONSTRAINT book_id;
       profile       postgres    false    181    175    1967         �           2606    98623    book_id    FK CONSTRAINT     d   ALTER TABLE ONLY barcode
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);
 :   ALTER TABLE ONLY profile.barcode DROP CONSTRAINT book_id;
       profile       postgres    false    195    175    1967         �           2606    98580    group_id    FK CONSTRAINT     h   ALTER TABLE ONLY "user"
    ADD CONSTRAINT group_id FOREIGN KEY (group_id) REFERENCES groups(group_id);
 :   ALTER TABLE ONLY profile."user" DROP CONSTRAINT group_id;
       profile       postgres    false    179    1974    185         �           2606    57405    user_id    FK CONSTRAINT     g   ALTER TABLE ONLY feedback
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);
 ;   ALTER TABLE ONLY profile.feedback DROP CONSTRAINT user_id;
       profile       postgres    false    1985    185    187         �           2606    57435    user_id    FK CONSTRAINT     o   ALTER TABLE ONLY request_new_book
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);
 C   ALTER TABLE ONLY profile.request_new_book DROP CONSTRAINT user_id;
       profile       postgres    false    185    189    1985         �           2606    65604    user_id    FK CONSTRAINT     j   ALTER TABLE ONLY borrow_book
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);
 >   ALTER TABLE ONLY profile.borrow_book DROP CONSTRAINT user_id;
       profile       postgres    false    185    177    1985         �           2606    65627    user_id    FK CONSTRAINT     k   ALTER TABLE ONLY reserve_book
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);
 ?   ALTER TABLE ONLY profile.reserve_book DROP CONSTRAINT user_id;
       profile       postgres    false    185    181    1985         �           2606    65638    user_id    FK CONSTRAINT     j   ALTER TABLE ONLY return_book
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);
 >   ALTER TABLE ONLY profile.return_book DROP CONSTRAINT user_id;
       profile       postgres    false    183    1985    185         �           2606    90365    user_id    FK CONSTRAINT     d   ALTER TABLE ONLY fines
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);
 8   ALTER TABLE ONLY profile.fines DROP CONSTRAINT user_id;
       profile       postgres    false    1985    193    185                                                 2120.dat                                                                                            0000600 0004000 0002000 00000000173 12574536427 014256  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Ram Pd. Dahal
2	Shyam thapa
3	Hari Adhikari
4	John Lovato
5	Rock Lopez
6	Krishna Bahadur Bhattarai
7	Narayan Poudel
\.


                                                                                                                                                                                                                                                                                                                                                                                                     2142.dat                                                                                            0000600 0004000 0002000 00000000005 12574536427 014254  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2122.dat                                                                                            0000600 0004000 0002000 00000001037 12574536427 014260  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        4	play with physics	1	4	456	nima	15	Available	\N	15	\N
3	tour of physics	3	5	345	a.k. publications	11	Available	\N	11	\N
6	Test	4	1	3	Ekta	4	Available	\N	\N	\N
7	Barcode Test	4	\N	23	Ekta	4	Available	\N	\N	05-23-068
8	test	4	\N			\N		\N	\N	05-23-068
9	barcode test	4	\N			\N		\N	\N	K
11	book	5	\N			\N		\N	\N	K
12	barcode testing	5	\N			\N		\N	\N	123
1	textBook of physics	5	1	123	ekta	15	Available	\N	6	\N
2	principle of physics	4	3	234	bhudipuran	14	Available	\N	7	\N
5	walk through physics	2	2	567	shree chandeswor	18	Reserved	\N	7	\N
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 2124.dat                                                                                            0000600 0004000 0002000 00000000005 12574536427 014254  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2134.dat                                                                                            0000600 0004000 0002000 00000000171 12574536427 014261  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	\N	\N		
2	\N	\N		
3	\N	\N		
4	\N	\N		
5	\N	2015-08-30	testing feedback	test
6	\N	2015-08-30	testing feedback	test
\.


                                                                                                                                                                                                                                                                                                                                                                                                       2140.dat                                                                                            0000600 0004000 0002000 00000000015 12574536427 014253  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1	3	4
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   2126.dat                                                                                            0000600 0004000 0002000 00000000031 12574536427 014255  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Student
2	Teacher
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       2136.dat                                                                                            0000600 0004000 0002000 00000000005 12574536427 014257  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2128.dat                                                                                            0000600 0004000 0002000 00000003617 12574536427 014274  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        36	2015-08-29 14:32:33.559	1	2015-09-01	4
37	2015-08-29 14:39:42.722	3	2015-09-01	4
38	2015-08-29 14:39:44.228	4	2015-09-01	4
39	2015-08-29 14:39:59.358	2	2015-09-01	4
40	2015-08-29 14:40:03.232	2	2015-09-01	4
41	2015-08-29 14:40:08.346	5	2015-09-01	4
42	2015-08-29 14:40:12.22	5	2015-09-01	4
43	2015-08-29 14:40:14.739	5	2015-09-01	4
44	2015-08-29 14:40:18.107	5	2015-09-01	4
45	2015-08-29 14:40:31.257	5	2015-09-01	4
46	2015-08-29 14:40:34.364	5	2015-09-01	4
47	2015-08-29 14:40:36.692	5	2015-09-01	4
48	2015-08-29 14:41:32.159	2	2015-09-01	1
49	2015-08-29 14:42:08.152	1	2015-09-01	1
50	2015-08-29 14:42:09.766	3	2015-09-01	1
51	2015-08-29 16:10:04.641	4	2015-09-01	4
52	2015-08-29 16:10:05.169	4	2015-09-01	4
53	2015-08-29 16:10:05.423	4	2015-09-01	4
54	2015-08-29 16:10:05.682	4	2015-09-01	4
55	2015-08-29 16:10:05.981	4	2015-09-01	4
56	2015-08-29 16:10:06.288	4	2015-09-01	4
57	2015-08-29 16:10:06.55	4	2015-09-01	4
58	2015-08-29 16:10:06.955	4	2015-09-01	4
59	2015-08-29 16:11:10.669	3	2015-09-01	4
60	2015-08-29 16:11:10.955	3	2015-09-01	4
61	2015-08-29 16:11:11.183	3	2015-09-01	4
62	2015-08-29 16:11:11.408	3	2015-09-01	4
63	2015-08-29 16:11:11.641	3	2015-09-01	4
64	2015-08-29 16:11:11.943	3	2015-09-01	4
65	2015-08-29 16:11:12.161	3	2015-09-01	4
66	2015-08-29 16:11:12.456	3	2015-09-01	4
67	2015-08-30 08:48:49.607	2	2015-09-02	4
68	2015-08-30 15:44:35.133	1	2015-09-02	4
69	2015-08-31 08:01:27.054	2	2015-09-03	4
70	2015-08-31 08:01:30.807	1	2015-09-03	4
71	2015-08-31 08:02:48.664	1	2015-09-03	4
72	2015-08-31 08:04:51.319	2	2015-09-03	4
73	2015-09-01 15:53:57.158	1	2015-09-04	4
74	2015-09-01 19:55:56.856	2	2015-09-04	4
75	2015-09-02 09:28:30.772	4	2015-09-05	4
76	2015-09-02 09:28:36.979	4	2015-09-05	4
77	2015-09-02 10:46:17.303	4	2015-09-05	4
78	2015-09-02 15:05:25.054	4	2015-09-05	4
79	2015-09-02 15:05:31.606	4	2015-09-05	4
80	2015-09-02 15:05:42.522	4	2015-09-05	4
81	2015-09-07 13:07:44.035	3	2015-09-10	4
\.


                                                                                                                 2130.dat                                                                                            0000600 0004000 0002000 00000000005 12574536427 014251  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2132.dat                                                                                            0000600 0004000 0002000 00000001211 12574536427 014253  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        4	admin	admin		admin@gmail.com	everywhere	\N	admin	t	humanity	\N	\N
3	sailen	maharjan	9808520541	sailen_maharjan@gmail.com	lagankhel	\N	sailendra	t	\N	\N	\N
5	sujan	shakya	9811111111	sujan@hotmail.com	balkumari	\N	sujan	f	\N	\N	\N
13	nice	pass				\N		f	\N	\N	\N
15	astonishing	astonish				\N		f	\N	\N	\N
16	amazing	amaze				\N		f	\N	\N	\N
17	user	pass				\N		f	\N	\N	\N
18	krishna	god				\N		f	\N	\N	\N
2	shankar	poudel	9843230416	shankar.poudel93@gmail.com	imadol	\N	shankar	t	flick	\N	\N
12	very	good				\N		f	sam	\N	\N
14	very	nice				\N		f	sam	\N	\N
1	kshitij	horijon	9849824371	k.kshitij.k.khanal@gmail.com	khahare	\N	kshitij	t	home	\N	\N
\.


                                                                                                                                                                                                                                                                                                                                                                                       2139.dat                                                                                            0000600 0004000 0002000 00000033610 12574536427 014272  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        186	admin	2015-09-02
187	admin	2015-09-02
188	admin	2015-09-02
189	admin	2015-09-02
190	admin	2015-09-02
191	admin	2015-09-02
192	admin	2015-09-02
193	admin	2015-09-02
194	admin	2015-09-02
195	kshitij	2015-09-02
196	test	2015-09-02
197	admin	2015-09-02
198	shankar	2015-09-02
199	admin	2015-09-02
200	test	2015-09-02
201	admin	2015-09-02
202	admin	2015-09-02
203	admin	2015-09-02
204	admin	2015-09-02
205	admin	2015-09-02
206	test	2015-09-02
207	admin	2015-09-02
208	admin	2015-09-02
209	admin	2015-09-02
210	admin	2015-09-02
211	admin	2015-09-02
212	admin	2015-09-02
213	admin	2015-09-02
214	admin	2015-09-02
215	admin	2015-09-02
216	admin	2015-09-02
217	admin	2015-09-02
218	shankar	2015-09-02
219	admin	2015-09-02
220	admin	2015-09-02
221	admin	2015-09-02
222	admin	2015-09-02
223	admin	2015-09-02
224	admin	2015-09-02
225	admin	2015-09-02
226	admin	2015-09-02
227	admin	2015-09-02
228	admin	2015-09-02
229	admin	2015-09-02
230	admin	2015-09-02
231	admin	2015-09-02
232	admin	2015-09-02
233	admin	2015-09-02
234	fjslkj	2015-09-02
235	fjslkj	2015-09-02
236	fjslkj	2015-09-02
237	fjslkj	2015-09-02
238	fjslkj	2015-09-02
239	fjslkj	2015-09-02
240	test	2015-09-02
241	admin	2015-09-02
242	admin	2015-09-02
243	admin	2015-09-02
244	admin	2015-09-02
245	admin	2015-09-02
246	admin	2015-09-02
247	admin	2015-09-02
248	admin	2015-09-02
249	admin	2015-09-02
250	kshitij	2015-09-02
251	kshitij	2015-09-02
252	kshitij	2015-09-02
253	admin	2015-09-02
254	admin	2015-09-02
255	admin	2015-09-02
256	very	2015-09-02
257	admin	2015-09-02
258	admin	2015-09-02
259	admin	2015-09-02
260	admin	2015-09-02
261	admin	2015-09-02
262	admin	2015-09-02
263	admin	2015-09-02
264	admin	2015-09-02
265	admin	2015-09-02
266	admin	2015-09-02
267	admin	2015-09-02
268	admin	2015-09-02
269	admin	2015-09-02
270	admin	2015-09-02
271	admin	2015-09-02
272	admin	2015-09-02
273	admin	2015-09-02
274	admin	2015-09-02
275	admin	2015-09-02
276	admin	2015-09-02
277	admin	2015-09-02
278	admin	2015-09-02
279	admin	2015-09-02
280	admin	2015-09-02
281	admin	2015-09-02
282	admin	2015-09-02
283	admin	2015-09-02
284	admin	2015-09-02
286	admin	2015-09-02
287	admin	2015-09-02
288	admin	2015-09-02
289	admin	2015-09-02
290	kshitij	2015-09-02
291	admin	2015-09-02
292	admin	2015-09-02
293	admin	2015-09-02
294	admin	2015-09-02
295	admin	2015-09-02
296	admin	2015-09-02
297	fkdjsh	2015-09-02
298	kshitij	2015-09-02
299	admin	2015-09-02
300	admin	2015-09-02
301	admin	2015-09-02
302	kshitij	2015-09-02
303	admin	2015-09-02
304	admin	2015-09-02
305	admin	2015-09-02
306	admin	2015-09-02
307	admin	2015-09-02
308	kshitij	2015-09-02
309	kshitij	2015-09-02
310	admin	2015-09-02
311	admin	2015-09-02
312	test	2015-09-02
313	test	2015-09-02
314	very	2015-09-02
315	very	2015-09-02
316	kshitij	2015-09-02
317	kshitij	2015-09-02
318	very	2015-09-02
319	kshitij	2015-09-02
320	kshitij	2015-09-02
321	kshitij	2015-09-02
322	kshitij	2015-09-02
323	admin	2015-09-02
324	admin	2015-09-02
325	admin	2015-09-02
326	admin	2015-09-02
327	kshitij	2015-09-02
328	test	2015-09-02
329	test	2015-09-02
330	very	2015-09-02
331	very	2015-09-02
332	admin	2015-09-02
333	kshitij	2015-09-02
334	admin	2015-09-02
335	admin	2015-09-02
336	admin	2015-09-02
337	admin	2015-09-02
338	admin	2015-09-02
339	admin	2015-09-02
340	admin	2015-09-02
341	admin	2015-09-02
342	admin	2015-09-02
343	admin	2015-09-03
344	admin	2015-09-03
345	ADMIN	2015-09-03
346	ADMIN	2015-09-03
347	admin	2015-09-03
348	admin	2015-09-03
349	admin	2015-09-03
350	admin	2015-09-03
351	admin	2015-09-03
352	admin	2015-09-03
353	01-22-068	2015-09-03
354	admin	2015-09-03
355	admin	2015-09-03
356	admin	2015-09-03
357	admin	2015-09-03
358	admin	2015-09-03
359	admin	2015-09-03
360	admin	2015-09-03
361	admin	2015-09-03
362	admin	2015-09-03
363	admin	2015-09-03
364	admin	2015-09-03
365	admin	2015-09-03
366	admin	2015-09-03
367	admin	2015-09-03
368	admin	2015-09-03
369	admin	2015-09-03
370	admin	2015-09-03
371	admin	2015-09-03
372	admin	2015-09-03
373	admin	2015-09-03
374	admin	2015-09-03
375	admin	2015-09-03
376	admin	2015-09-03
377	admin	2015-09-03
378	admin	2015-09-03
379	admin	2015-09-03
380	admin	2015-09-03
381	admin	2015-09-03
382	admin	2015-09-03
383	admin	2015-09-03
384	admin	2015-09-03
385	admin	2015-09-03
386	admin	2015-09-03
387	admin	2015-09-03
388	admin	2015-09-03
389	admin	2015-09-03
390	admin	2015-09-03
391	admin	2015-09-03
392	admin	2015-09-03
393	admin	2015-09-03
394	admin	2015-09-03
395	admin	2015-09-03
396	admin	2015-09-03
397	admin	2015-09-03
398	admin	2015-09-03
399	admin	2015-09-03
400	admin	2015-09-03
401	admin	2015-09-03
402	admin	2015-09-03
403	admin	2015-09-03
404	admin	2015-09-03
405	admin	2015-09-03
406	admin	2015-09-03
407	admin	2015-09-03
408	admin	2015-09-03
409	admin	2015-09-03
410	admin	2015-09-03
411	admin	2015-09-03
412	admin	2015-09-03
413	admin	2015-09-03
414	admin	2015-09-03
415	admin	2015-09-03
416	admin	2015-09-03
417	admin	2015-09-03
418	admin	2015-09-03
419	admin	2015-09-03
420	admin	2015-09-03
421	admin	2015-09-03
422	admin	2015-09-03
423	admin	2015-09-03
424	admin	2015-09-03
425	admin	2015-09-03
426	admin	2015-09-03
427	admin	2015-09-03
428	admin	2015-09-03
429	admin	2015-09-03
430	kshitij	2015-09-03
431	admin	2015-09-03
432	admin	2015-09-03
433	admin	2015-09-03
434	admin	2015-09-03
435	admin	2015-09-03
436	admin	2015-09-03
437	admin	2015-09-03
438	admin	2015-09-03
439	admin	2015-09-03
440	admin	2015-09-03
441	admin	2015-09-03
442	admin	2015-09-03
443	admin	2015-09-04
444	admin	2015-09-04
445	admin	2015-09-04
446	admin	2015-09-04
447	admin	2015-09-04
448	admin	2015-09-04
449	admin	2015-09-04
450	admin	2015-09-04
451	admin	2015-09-04
452	adkldjfl	2015-09-04
453	adkldjfl	2015-09-04
454	adkldjfl	2015-09-04
455	admin	2015-09-04
456	admin	2015-09-04
457	admin	2015-09-04
458	admin	2015-09-04
459	admin	2015-09-04
460	admin	2015-09-04
461	admin	2015-09-04
462	admin	2015-09-04
463	admin	2015-09-04
464	admin	2015-09-04
465	admin	2015-09-04
466	admin	2015-09-04
467	admin	2015-09-04
468	admin	2015-09-04
469	admin	2015-09-04
470	admin	2015-09-04
471	admin	2015-09-04
472	admin	2015-09-04
473	admin	2015-09-04
474	admin	2015-09-04
475	admin	2015-09-04
476	admin	2015-09-04
477	admin	2015-09-04
478	admin	2015-09-04
479	admin	2015-09-04
480	admin	2015-09-04
481	admin	2015-09-04
482	admin	2015-09-04
483	admin	2015-09-04
484	admin	2015-09-04
485	admin	2015-09-04
486	admin	2015-09-04
487	admin	2015-09-04
488	admin	2015-09-04
489	admin	2015-09-04
490	admin	2015-09-04
491	admin	2015-09-04
492	admin	2015-09-04
493	admin	2015-09-04
494	admin	2015-09-04
495	admin	2015-09-04
496	admin	2015-09-04
497	admin	2015-09-04
498	admin	2015-09-04
499	admin	2015-09-04
500	admin	2015-09-04
501	admin	2015-09-04
502	admin	2015-09-04
503	admin	2015-09-04
504	admin	2015-09-04
505	admin	2015-09-04
506	admin	2015-09-04
507	admin	2015-09-04
508	admin	2015-09-04
509	admin	2015-09-04
510	admin	2015-09-04
511	admin	2015-09-04
512	admin	2015-09-04
513	admin	2015-09-04
514	admin	2015-09-04
515	admin	2015-09-04
516	admin	2015-09-04
517	admin	2015-09-04
518	admin	2015-09-04
519	admin	2015-09-04
520	admin	2015-09-04
521	admin	2015-09-04
522	admin	2015-09-04
523	admin	2015-09-04
524	admin	2015-09-04
525	admin	2015-09-04
526	admin	2015-09-04
527	admin	2015-09-04
528	admin	2015-09-04
529	admin	2015-09-04
530	admin	2015-09-04
531	admin	2015-09-05
532	admin	2015-09-05
533	admin	2015-09-05
534	admin	2015-09-05
535	admin	2015-09-05
536	admin	2015-09-05
537	admin	2015-09-05
538	admin	2015-09-05
539	admin	2015-09-05
540	admin	2015-09-05
541	admin	2015-09-05
542	admin	2015-09-05
543	admin	2015-09-05
544	admin	2015-09-05
545	admin	2015-09-05
546	very	2015-09-05
547	very	2015-09-05
548	admin	2015-09-05
549	very	2015-09-05
550	admin	2015-09-05
551	very	2015-09-05
552	admin	2015-09-05
553	admin	2015-09-05
554	admin	2015-09-05
555	admin	2015-09-05
556	admin	2015-09-05
557	admin	2015-09-05
558	admin	2015-09-05
559	admin	2015-09-05
560	admin	2015-09-05
561	admin	2015-09-05
562	admin	2015-09-05
563	admin	2015-09-05
564	admin	2015-09-05
565	admin	2015-09-05
566	admin	2015-09-05
567	admin	2015-09-05
568	admin	2015-09-05
569	admin	2015-09-05
570	admin	2015-09-05
571	admin	2015-09-05
572	admin	2015-09-05
573	admin	2015-09-05
574	admin	2015-09-05
575	admin	2015-09-05
576	admin	2015-09-05
577	admin	2015-09-05
578	admin	2015-09-05
579	admin	2015-09-05
580	admin	2015-09-05
581	admin	2015-09-05
582	kshitij	2015-09-05
583	admin	2015-09-05
584	admin	2015-09-05
585	kshitij	2015-09-05
586	kshitij	2015-09-05
587	very	2015-09-05
588	shankar	2015-09-05
589	shankar	2015-09-05
590	admin	2015-09-05
591	shankar	2015-09-05
592	admin	2015-09-05
593	admin	2015-09-05
594	shankar	2015-09-05
595	admin	2015-09-05
596	admin	2015-09-05
597	shankar	2015-09-05
598	shankar	2015-09-05
599	admin	2015-09-05
600	admin	2015-09-05
601	shankar	2015-09-05
602	shankar	2015-09-05
603	user	2015-09-05
604	shankar	2015-09-05
605	amazing	2015-09-05
606	amazing	2015-09-05
607	shankar	2015-09-05
608	shankar	2015-09-05
609	shankar	2015-09-05
610	admin	2015-09-05
611	kshitij	2015-09-05
612	shankar	2015-09-05
613	kshitij	2015-09-06
614	admin	2015-09-06
615	admin	2015-09-06
616	admin	2015-09-06
617	admin	2015-09-06
618	admin	2015-09-06
619	admin	2015-09-06
620	admin	2015-09-06
621	admin	2015-09-06
622	admin	2015-09-06
623	admin	2015-09-06
624	admin	2015-09-06
625	admin	2015-09-06
626	admin	2015-09-06
627	admin	2015-09-06
628	admin	2015-09-06
629	admin	2015-09-06
630	admin	2015-09-06
631	admin	2015-09-06
632	admin	2015-09-06
633	admin	2015-09-06
634	admin	2015-09-06
635	admin	2015-09-06
636	admin	2015-09-06
637	admin	2015-09-06
638	admin	2015-09-06
639	doggy vai	2015-09-06
640	shankar	2015-09-06
641	shankar	2015-09-06
642	kshitij	2015-09-06
643	admin	2015-09-06
644	admin	2015-09-06
645	admin	2015-09-06
646	admin	2015-09-06
647	admin	2015-09-06
648	admin	2015-09-06
649	admin	2015-09-06
650	admin	2015-09-06
651	admin	2015-09-06
652	admin	2015-09-06
653	admin	2015-09-06
654	admin	2015-09-06
655	admin	2015-09-06
656	admin	2015-09-06
657	admin	2015-09-06
658	admin	2015-09-06
659	admin	2015-09-06
660	admin	2015-09-06
661	admin	2015-09-06
662	admin	2015-09-06
663	admin	2015-09-06
664	admin	2015-09-06
665	admin	2015-09-06
666	admin	2015-09-06
667	admin	2015-09-06
668	admin	2015-09-06
669	admin	2015-09-06
670	admin	2015-09-06
671	admin	2015-09-06
672	admin	2015-09-06
673	admin	2015-09-06
674	admin	2015-09-06
675	admin	2015-09-06
676	admin	2015-09-06
677	admin	2015-09-06
678	admin	2015-09-06
679	admin	2015-09-06
680	admin	2015-09-06
681	admin	2015-09-06
682	admin	2015-09-06
683	kshitij	2015-09-06
684	admin	2015-09-06
685	admin	2015-09-06
686	admin	2015-09-06
687	admin	2015-09-06
688	admin	2015-09-06
689	admin	2015-09-06
690	admin	2015-09-06
691	admin	2015-09-06
692	admin	2015-09-06
693	admin	2015-09-06
694	admin	2015-09-06
695	kshitij	2015-09-06
696	kshitij	2015-09-06
697	shankar	2015-09-06
698	admin	2015-09-06
699	kshitij	2015-09-06
700	admin	2015-09-07
701	admin	2015-09-07
702	admin	2015-09-07
703	admin	2015-09-07
704	admin	2015-09-07
705	admin	2015-09-07
706	admin	2015-09-07
707	admin	2015-09-07
708	admin	2015-09-07
709	admin	2015-09-07
710	admin	2015-09-07
711	admin	2015-09-07
712	admin	2015-09-07
713	admin	2015-09-07
714	admin	2015-09-07
715	admin	2015-09-07
716	admin	2015-09-07
717	admin	2015-09-07
718	admin	2015-09-07
719	admin	2015-09-07
720	admin	2015-09-07
721	admin	2015-09-07
722	admin	2015-09-07
723	admin	2015-09-07
724	admin	2015-09-07
725	admin	2015-09-07
726	admin	2015-09-07
727	admin	2015-09-07
728	admin	2015-09-07
729	kshitij	2015-09-07
730	admin	2015-09-07
731	admin	2015-09-07
732	admin	2015-09-07
733	admin	2015-09-07
734	kshitij	2015-09-07
735	admin	2015-09-07
736	admin	2015-09-07
737	admin	2015-09-07
738	admin	2015-09-07
739	kshitij	2015-09-07
740	kshitij	2015-09-07
741	admin	2015-09-07
742	admin	2015-09-07
743	admin	2015-09-07
744	admin	2015-09-07
745	admin	2015-09-07
746	admin	2015-09-07
747	admin	2015-09-07
748	admin	2015-09-07
749	admin	2015-09-07
750	admin	2015-09-07
751	admin	2015-09-07
752	admin	2015-09-07
753	admin	2015-09-07
754	admin	2015-09-07
755	admin	2015-09-07
756	admin	2015-09-07
757	admin	2015-09-07
758	admin	2015-09-07
759	admin	2015-09-07
760	admin	2015-09-07
761	admin	2015-09-07
762	admin	2015-09-07
763	admin	2015-09-07
764	admin	2015-09-07
765	ajdlf	2015-09-07
766	admin	2015-09-07
767	admin	2015-09-07
768	kshitij	2015-09-07
769	admin	2015-09-07
770	kshitij	2015-09-07
771	admin	2015-09-07
772	admin	2015-09-07
773	shankar	2015-09-07
774	admin	2015-09-08
775	admin	2015-09-08
776	admin	2015-09-08
777	admin	2015-09-08
778	admin	2015-09-08
779	admin	2015-09-08
780	admin	2015-09-08
781	admin	2015-09-08
782	admin	2015-09-08
783	admin	2015-09-08
784	admin	2015-09-08
785	admin	2015-09-08
786	admin	2015-09-08
787	jfdlkjs	2015-09-08
788	jfdlkjs	2015-09-08
789	jfdlkjs	2015-09-08
790	admin	2015-09-08
791	admin	2015-09-08
792	admin	2015-09-08
793	admin	2015-09-08
794	admin	2015-09-08
795	admin	2015-09-08
796	admin	2015-09-08
797	admin	2015-09-08
798	admin	2015-09-08
799	admin	2015-09-08
800	admin	2015-09-08
801	admin	2015-09-08
802	admin	2015-09-08
803	admin	2015-09-08
804	admin	2015-09-08
805	admin	2015-09-08
806	admin	2015-09-08
807	admin	2015-09-08
808	admin	2015-09-08
809	admin	2015-09-08
810	admin	2015-09-09
811	admin	2015-09-09
812	admin	2015-09-09
813	admin	2015-09-09
814	admin	2015-09-09
815	admin	2015-09-09
816	admin	2015-09-09
817	admin	2015-09-09
818	admin	2015-09-09
819	admin	2015-09-09
820	admin	2015-09-09
821	admin	2015-09-09
822	admin	2015-09-10
823	admin	2015-09-10
824	admin	2015-09-10
825	admin	2015-09-10
826	admin	2015-09-10
827	admin	2015-09-10
828	admin	2015-09-10
829	admin	2015-09-10
830	admin	2015-09-10
831	admin	2015-09-10
832	admin	2015-09-10
833	admin	2015-09-10
834	admin	2015-09-10
835	admin	2015-09-10
836	admin	2015-09-10
837	admin	2015-09-10
838	admin	2015-09-10
839	admin	2015-09-10
840	admin	2015-09-10
841	admin	2015-09-10
842	admin	2015-09-10
843	admin	2015-09-11
844	admin	2015-09-11
845	admin	2015-09-11
846	admin	2015-09-11
847	admin	2015-09-11
848	admin	2015-09-11
849	admin	2015-09-11
850	admin	2015-09-11
851	admin	2015-09-11
852	admin	2015-09-11
853	admin	2015-09-11
854	admin	2015-09-11
855	admin	2015-09-11
856	admin	2015-09-11
857	admin	2015-09-11
\.


                                                                                                                        restore.sql                                                                                         0000600 0004000 0002000 00000065122 12574536427 015411  0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = profile, pg_catalog;

ALTER TABLE ONLY profile.fines DROP CONSTRAINT user_id;
ALTER TABLE ONLY profile.return_book DROP CONSTRAINT user_id;
ALTER TABLE ONLY profile.reserve_book DROP CONSTRAINT user_id;
ALTER TABLE ONLY profile.borrow_book DROP CONSTRAINT user_id;
ALTER TABLE ONLY profile.request_new_book DROP CONSTRAINT user_id;
ALTER TABLE ONLY profile.feedback DROP CONSTRAINT user_id;
ALTER TABLE ONLY profile."user" DROP CONSTRAINT group_id;
ALTER TABLE ONLY profile.barcode DROP CONSTRAINT book_id;
ALTER TABLE ONLY profile.reserve_book DROP CONSTRAINT book_id;
ALTER TABLE ONLY profile.return_book DROP CONSTRAINT book_id;
ALTER TABLE ONLY profile.borrow_book DROP CONSTRAINT book_id;
ALTER TABLE ONLY profile.fines DROP CONSTRAINT book_id;
ALTER TABLE ONLY profile.book DROP CONSTRAINT author_id;
DROP INDEX profile.fkis_book_id;
DROP INDEX profile.fkire_user_id;
DROP INDEX profile.fkir_user_id;
DROP INDEX profile.fkibb_user_id;
DROP INDEX profile.fkia_book_id;
DROP INDEX profile.fki_user_id;
DROP INDEX profile.fki_group_id;
DROP INDEX profile.fki_book_id;
DROP INDEX profile.fki_author_id;
DROP INDEX profile.fk_user_id;
ALTER TABLE ONLY profile."user" DROP CONSTRAINT user_id;
ALTER TABLE ONLY profile.user_history DROP CONSTRAINT user_history_id;
ALTER TABLE ONLY profile.return_book DROP CONSTRAINT return_book_id;
ALTER TABLE ONLY profile.reserve_book DROP CONSTRAINT reserve_book_id;
ALTER TABLE ONLY profile.request_new_book DROP CONSTRAINT request_new_book_id;
ALTER TABLE ONLY profile.groups DROP CONSTRAINT group_id;
ALTER TABLE ONLY profile.fines DROP CONSTRAINT fine_id;
ALTER TABLE ONLY profile.feedback DROP CONSTRAINT feedback_id;
ALTER TABLE ONLY profile.borrow_book DROP CONSTRAINT borrow_book_id;
ALTER TABLE ONLY profile.book DROP CONSTRAINT book_id;
ALTER TABLE ONLY profile.barcode DROP CONSTRAINT barcode_id;
ALTER TABLE ONLY profile.author DROP CONSTRAINT author_id;
ALTER TABLE profile.user_history ALTER COLUMN user_history_id DROP DEFAULT;
ALTER TABLE profile."user" ALTER COLUMN user_id DROP DEFAULT;
ALTER TABLE profile.return_book ALTER COLUMN return_book_id DROP DEFAULT;
ALTER TABLE profile.reserve_book ALTER COLUMN reserve_book_id DROP DEFAULT;
ALTER TABLE profile.request_new_book ALTER COLUMN request_new_book_id DROP DEFAULT;
ALTER TABLE profile.groups ALTER COLUMN group_id DROP DEFAULT;
ALTER TABLE profile.fines ALTER COLUMN fine_id DROP DEFAULT;
ALTER TABLE profile.feedback ALTER COLUMN feedback_id DROP DEFAULT;
ALTER TABLE profile.borrow_book ALTER COLUMN borrow_book_id DROP DEFAULT;
ALTER TABLE profile.book ALTER COLUMN book_id DROP DEFAULT;
ALTER TABLE profile.author ALTER COLUMN author_id DROP DEFAULT;
DROP SEQUENCE profile.user_user_id_seq;
DROP SEQUENCE profile.user_history_user_history_id_seq;
DROP TABLE profile.user_history;
DROP TABLE profile."user";
DROP SEQUENCE profile.return_book_return_book_id_seq;
DROP TABLE profile.return_book;
DROP SEQUENCE profile.reserve_book_reserve_book_id_seq;
DROP TABLE profile.reserve_book;
DROP SEQUENCE profile.request_new_book_request_new_book_id_seq;
DROP TABLE profile.request_new_book;
DROP SEQUENCE profile.group_group_id_seq;
DROP TABLE profile.groups;
DROP SEQUENCE profile.fines_fine_id_seq;
DROP TABLE profile.fines;
DROP SEQUENCE profile.feedback_feedback_id_seq;
DROP TABLE profile.feedback;
DROP SEQUENCE profile.borrow_book_borrow_book_id_seq;
DROP TABLE profile.borrow_book;
DROP SEQUENCE profile.book_book_id_seq;
DROP TABLE profile.book;
DROP TABLE profile.barcode;
DROP SEQUENCE profile.author_author_id_seq;
DROP TABLE profile.author;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
DROP SCHEMA profile;
--
-- Name: profile; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA profile;


ALTER SCHEMA profile OWNER TO postgres;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = profile, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: author; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE author (
    author_id integer NOT NULL,
    author_name character varying(255) NOT NULL
);


ALTER TABLE author OWNER TO postgres;

--
-- Name: author_author_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE author_author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE author_author_id_seq OWNER TO postgres;

--
-- Name: author_author_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE author_author_id_seq OWNED BY author.author_id;


--
-- Name: barcode; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE barcode (
    barcode_id integer NOT NULL,
    book_id integer NOT NULL
);


ALTER TABLE barcode OWNER TO postgres;

--
-- Name: book; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE book (
    book_id integer NOT NULL,
    book_name character varying(255) NOT NULL,
    book_edition integer NOT NULL,
    author_id integer,
    book_number character varying(255),
    publisher character varying(255),
    quantity integer,
    book_status character varying(255),
    borrowed_book_count integer,
    reserve_book_count integer,
    barcode_id character varying(255)
);


ALTER TABLE book OWNER TO postgres;

--
-- Name: book_book_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE book_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE book_book_id_seq OWNER TO postgres;

--
-- Name: book_book_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE book_book_id_seq OWNED BY book.book_id;


--
-- Name: borrow_book; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE borrow_book (
    borrow_book_id integer NOT NULL,
    borrow_date date NOT NULL,
    expiry_date date NOT NULL,
    book_id integer NOT NULL,
    user_id integer
);


ALTER TABLE borrow_book OWNER TO postgres;

--
-- Name: borrow_book_borrow_book_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE borrow_book_borrow_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE borrow_book_borrow_book_id_seq OWNER TO postgres;

--
-- Name: borrow_book_borrow_book_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE borrow_book_borrow_book_id_seq OWNED BY borrow_book.borrow_book_id;


--
-- Name: feedback; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE feedback (
    feedback_id integer NOT NULL,
    user_id integer,
    date date,
    description character varying(255),
    subject character varying(255)
);


ALTER TABLE feedback OWNER TO postgres;

--
-- Name: feedback_feedback_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE feedback_feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feedback_feedback_id_seq OWNER TO postgres;

--
-- Name: feedback_feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE feedback_feedback_id_seq OWNED BY feedback.feedback_id;


--
-- Name: fines; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE fines (
    fine_id integer NOT NULL,
    book_id integer,
    user_id integer,
    fineamount integer
);


ALTER TABLE fines OWNER TO postgres;

--
-- Name: fines_fine_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE fines_fine_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE fines_fine_id_seq OWNER TO postgres;

--
-- Name: fines_fine_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE fines_fine_id_seq OWNED BY fines.fine_id;


--
-- Name: groups; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE groups (
    group_id integer NOT NULL,
    groupname character varying(255) NOT NULL
);


ALTER TABLE groups OWNER TO postgres;

--
-- Name: group_group_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE group_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE group_group_id_seq OWNER TO postgres;

--
-- Name: group_group_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE group_group_id_seq OWNED BY groups.group_id;


--
-- Name: request_new_book; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE request_new_book (
    request_new_book_id integer NOT NULL,
    book_name character varying(255),
    author character varying(255),
    publisher character varying(255),
    edition integer,
    user_id integer
);


ALTER TABLE request_new_book OWNER TO postgres;

--
-- Name: request_new_book_request_new_book_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE request_new_book_request_new_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE request_new_book_request_new_book_id_seq OWNER TO postgres;

--
-- Name: request_new_book_request_new_book_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE request_new_book_request_new_book_id_seq OWNED BY request_new_book.request_new_book_id;


--
-- Name: reserve_book; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE reserve_book (
    reserve_book_id integer NOT NULL,
    reserve_start_date timestamp without time zone DEFAULT now() NOT NULL,
    book_id integer NOT NULL,
    reserve_finish_date date,
    user_id integer
);


ALTER TABLE reserve_book OWNER TO postgres;

--
-- Name: reserve_book_reserve_book_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE reserve_book_reserve_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reserve_book_reserve_book_id_seq OWNER TO postgres;

--
-- Name: reserve_book_reserve_book_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE reserve_book_reserve_book_id_seq OWNED BY reserve_book.reserve_book_id;


--
-- Name: return_book; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE return_book (
    return_book_id integer NOT NULL,
    return_date date NOT NULL,
    book_id integer NOT NULL,
    user_id integer
);


ALTER TABLE return_book OWNER TO postgres;

--
-- Name: return_book_return_book_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE return_book_return_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE return_book_return_book_id_seq OWNER TO postgres;

--
-- Name: return_book_return_book_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE return_book_return_book_id_seq OWNED BY return_book.return_book_id;


--
-- Name: user; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE "user" (
    user_id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    contact character varying,
    email character varying,
    address character varying(255),
    image bytea,
    name character varying(255),
    status boolean,
    theme character varying(255),
    group_id integer,
    barcode_id integer
);


ALTER TABLE "user" OWNER TO postgres;

--
-- Name: user_history; Type: TABLE; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE TABLE user_history (
    user_history_id integer NOT NULL,
    username character varying,
    time_stamp date DEFAULT now()
);


ALTER TABLE user_history OWNER TO postgres;

--
-- Name: user_history_user_history_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE user_history_user_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_history_user_history_id_seq OWNER TO postgres;

--
-- Name: user_history_user_history_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE user_history_user_history_id_seq OWNED BY user_history.user_history_id;


--
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: profile; Owner: postgres
--

CREATE SEQUENCE user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_user_id_seq OWNER TO postgres;

--
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: profile; Owner: postgres
--

ALTER SEQUENCE user_user_id_seq OWNED BY "user".user_id;


--
-- Name: author_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY author ALTER COLUMN author_id SET DEFAULT nextval('author_author_id_seq'::regclass);


--
-- Name: book_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY book ALTER COLUMN book_id SET DEFAULT nextval('book_book_id_seq'::regclass);


--
-- Name: borrow_book_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY borrow_book ALTER COLUMN borrow_book_id SET DEFAULT nextval('borrow_book_borrow_book_id_seq'::regclass);


--
-- Name: feedback_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY feedback ALTER COLUMN feedback_id SET DEFAULT nextval('feedback_feedback_id_seq'::regclass);


--
-- Name: fine_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY fines ALTER COLUMN fine_id SET DEFAULT nextval('fines_fine_id_seq'::regclass);


--
-- Name: group_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY groups ALTER COLUMN group_id SET DEFAULT nextval('group_group_id_seq'::regclass);


--
-- Name: request_new_book_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY request_new_book ALTER COLUMN request_new_book_id SET DEFAULT nextval('request_new_book_request_new_book_id_seq'::regclass);


--
-- Name: reserve_book_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY reserve_book ALTER COLUMN reserve_book_id SET DEFAULT nextval('reserve_book_reserve_book_id_seq'::regclass);


--
-- Name: return_book_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY return_book ALTER COLUMN return_book_id SET DEFAULT nextval('return_book_return_book_id_seq'::regclass);


--
-- Name: user_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY "user" ALTER COLUMN user_id SET DEFAULT nextval('user_user_id_seq'::regclass);


--
-- Name: user_history_id; Type: DEFAULT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY user_history ALTER COLUMN user_history_id SET DEFAULT nextval('user_history_user_history_id_seq'::regclass);


--
-- Data for Name: author; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY author (author_id, author_name) FROM stdin;
\.
COPY author (author_id, author_name) FROM '$$PATH$$/2120.dat';

--
-- Name: author_author_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('author_author_id_seq', 7, true);


--
-- Data for Name: barcode; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY barcode (barcode_id, book_id) FROM stdin;
\.
COPY barcode (barcode_id, book_id) FROM '$$PATH$$/2142.dat';

--
-- Data for Name: book; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY book (book_id, book_name, book_edition, author_id, book_number, publisher, quantity, book_status, borrowed_book_count, reserve_book_count, barcode_id) FROM stdin;
\.
COPY book (book_id, book_name, book_edition, author_id, book_number, publisher, quantity, book_status, borrowed_book_count, reserve_book_count, barcode_id) FROM '$$PATH$$/2122.dat';

--
-- Name: book_book_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('book_book_id_seq', 12, true);


--
-- Data for Name: borrow_book; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY borrow_book (borrow_book_id, borrow_date, expiry_date, book_id, user_id) FROM stdin;
\.
COPY borrow_book (borrow_book_id, borrow_date, expiry_date, book_id, user_id) FROM '$$PATH$$/2124.dat';

--
-- Name: borrow_book_borrow_book_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('borrow_book_borrow_book_id_seq', 5, true);


--
-- Data for Name: feedback; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY feedback (feedback_id, user_id, date, description, subject) FROM stdin;
\.
COPY feedback (feedback_id, user_id, date, description, subject) FROM '$$PATH$$/2134.dat';

--
-- Name: feedback_feedback_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('feedback_feedback_id_seq', 6, true);


--
-- Data for Name: fines; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY fines (fine_id, book_id, user_id, fineamount) FROM stdin;
\.
COPY fines (fine_id, book_id, user_id, fineamount) FROM '$$PATH$$/2140.dat';

--
-- Name: fines_fine_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('fines_fine_id_seq', 3, true);


--
-- Name: group_group_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('group_group_id_seq', 2, true);


--
-- Data for Name: groups; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY groups (group_id, groupname) FROM stdin;
\.
COPY groups (group_id, groupname) FROM '$$PATH$$/2126.dat';

--
-- Data for Name: request_new_book; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY request_new_book (request_new_book_id, book_name, author, publisher, edition, user_id) FROM stdin;
\.
COPY request_new_book (request_new_book_id, book_name, author, publisher, edition, user_id) FROM '$$PATH$$/2136.dat';

--
-- Name: request_new_book_request_new_book_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('request_new_book_request_new_book_id_seq', 1, false);


--
-- Data for Name: reserve_book; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY reserve_book (reserve_book_id, reserve_start_date, book_id, reserve_finish_date, user_id) FROM stdin;
\.
COPY reserve_book (reserve_book_id, reserve_start_date, book_id, reserve_finish_date, user_id) FROM '$$PATH$$/2128.dat';

--
-- Name: reserve_book_reserve_book_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('reserve_book_reserve_book_id_seq', 81, true);


--
-- Data for Name: return_book; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY return_book (return_book_id, return_date, book_id, user_id) FROM stdin;
\.
COPY return_book (return_book_id, return_date, book_id, user_id) FROM '$$PATH$$/2130.dat';

--
-- Name: return_book_return_book_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('return_book_return_book_id_seq', 1, false);


--
-- Data for Name: user; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY "user" (user_id, username, password, contact, email, address, image, name, status, theme, group_id, barcode_id) FROM stdin;
\.
COPY "user" (user_id, username, password, contact, email, address, image, name, status, theme, group_id, barcode_id) FROM '$$PATH$$/2132.dat';

--
-- Data for Name: user_history; Type: TABLE DATA; Schema: profile; Owner: postgres
--

COPY user_history (user_history_id, username, time_stamp) FROM stdin;
\.
COPY user_history (user_history_id, username, time_stamp) FROM '$$PATH$$/2139.dat';

--
-- Name: user_history_user_history_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('user_history_user_history_id_seq', 857, true);


--
-- Name: user_user_id_seq; Type: SEQUENCE SET; Schema: profile; Owner: postgres
--

SELECT pg_catalog.setval('user_user_id_seq', 18, true);


--
-- Name: author_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY author
    ADD CONSTRAINT author_id PRIMARY KEY (author_id);


--
-- Name: barcode_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY barcode
    ADD CONSTRAINT barcode_id PRIMARY KEY (barcode_id);


--
-- Name: book_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY book
    ADD CONSTRAINT book_id PRIMARY KEY (book_id);


--
-- Name: borrow_book_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY borrow_book
    ADD CONSTRAINT borrow_book_id PRIMARY KEY (borrow_book_id);


--
-- Name: feedback_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT feedback_id PRIMARY KEY (feedback_id);


--
-- Name: fine_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fines
    ADD CONSTRAINT fine_id PRIMARY KEY (fine_id);


--
-- Name: group_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT group_id PRIMARY KEY (group_id);


--
-- Name: request_new_book_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY request_new_book
    ADD CONSTRAINT request_new_book_id PRIMARY KEY (request_new_book_id);


--
-- Name: reserve_book_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY reserve_book
    ADD CONSTRAINT reserve_book_id PRIMARY KEY (reserve_book_id);


--
-- Name: return_book_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY return_book
    ADD CONSTRAINT return_book_id PRIMARY KEY (return_book_id);


--
-- Name: user_history_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_history
    ADD CONSTRAINT user_history_id PRIMARY KEY (user_history_id);


--
-- Name: user_id; Type: CONSTRAINT; Schema: profile; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_id PRIMARY KEY (user_id);


--
-- Name: fk_user_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_user_id ON request_new_book USING btree (user_id);


--
-- Name: fki_author_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_author_id ON book USING btree (author_id);


--
-- Name: fki_book_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_book_id ON borrow_book USING btree (book_id);


--
-- Name: fki_group_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_group_id ON "user" USING btree (group_id);


--
-- Name: fki_user_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_user_id ON feedback USING btree (user_id);


--
-- Name: fkia_book_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fkia_book_id ON reserve_book USING btree (book_id);


--
-- Name: fkibb_user_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fkibb_user_id ON borrow_book USING btree (user_id);


--
-- Name: fkir_user_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fkir_user_id ON reserve_book USING btree (user_id);


--
-- Name: fkire_user_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fkire_user_id ON return_book USING btree (user_id);


--
-- Name: fkis_book_id; Type: INDEX; Schema: profile; Owner: postgres; Tablespace: 
--

CREATE INDEX fkis_book_id ON return_book USING btree (book_id);


--
-- Name: author_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY book
    ADD CONSTRAINT author_id FOREIGN KEY (author_id) REFERENCES author(author_id);


--
-- Name: book_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY fines
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);


--
-- Name: book_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY borrow_book
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);


--
-- Name: book_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY return_book
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);


--
-- Name: book_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY reserve_book
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);


--
-- Name: book_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY barcode
    ADD CONSTRAINT book_id FOREIGN KEY (book_id) REFERENCES book(book_id);


--
-- Name: group_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT group_id FOREIGN KEY (group_id) REFERENCES groups(group_id);


--
-- Name: user_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- Name: user_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY request_new_book
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- Name: user_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY borrow_book
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- Name: user_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY reserve_book
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- Name: user_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY return_book
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- Name: user_id; Type: FK CONSTRAINT; Schema: profile; Owner: postgres
--

ALTER TABLE ONLY fines
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              