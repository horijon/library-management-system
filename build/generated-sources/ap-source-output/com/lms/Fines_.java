package com.lms;

import com.lms.Book;
import com.lms.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(Fines.class)
public class Fines_ { 

    public static volatile SingularAttribute<Fines, Integer> fineamount;
    public static volatile SingularAttribute<Fines, User> userId;
    public static volatile SingularAttribute<Fines, Book> bookId;
    public static volatile SingularAttribute<Fines, Integer> fineId;

}