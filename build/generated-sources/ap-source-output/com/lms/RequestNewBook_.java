package com.lms;

import com.lms.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(RequestNewBook.class)
public class RequestNewBook_ { 

    public static volatile SingularAttribute<RequestNewBook, String> author;
    public static volatile SingularAttribute<RequestNewBook, Integer> edition;
    public static volatile SingularAttribute<RequestNewBook, User> userId;
    public static volatile SingularAttribute<RequestNewBook, Integer> requestNewBookId;
    public static volatile SingularAttribute<RequestNewBook, String> bookName;
    public static volatile SingularAttribute<RequestNewBook, String> publisher;

}