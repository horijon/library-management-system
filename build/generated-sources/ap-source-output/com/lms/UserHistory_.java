package com.lms;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(UserHistory.class)
public class UserHistory_ { 

    public static volatile SingularAttribute<UserHistory, String> username;
    public static volatile SingularAttribute<UserHistory, Date> timeStamp;
    public static volatile SingularAttribute<UserHistory, Integer> userHistoryId;

}