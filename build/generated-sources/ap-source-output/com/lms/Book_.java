package com.lms;

import com.lms.Author;
import com.lms.Barcode;
import com.lms.BorrowBook;
import com.lms.Fines;
import com.lms.ReserveBook;
import com.lms.ReturnBook;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(Book.class)
public class Book_ { 

    public static volatile SingularAttribute<Book, String> barcodeId;
    public static volatile ListAttribute<Book, ReserveBook> reserveBookList;
    public static volatile ListAttribute<Book, ReturnBook> returnBookList;
    public static volatile SingularAttribute<Book, Integer> bookId;
    public static volatile ListAttribute<Book, Fines> finesList;
    public static volatile SingularAttribute<Book, String> bookNumber;
    public static volatile ListAttribute<Book, BorrowBook> borrowBookList;
    public static volatile SingularAttribute<Book, Author> authorId;
    public static volatile SingularAttribute<Book, Integer> bookEdition;
    public static volatile SingularAttribute<Book, String> publisher;
    public static volatile SingularAttribute<Book, String> bookStatus;
    public static volatile SingularAttribute<Book, Integer> reserveBookCount;
    public static volatile SingularAttribute<Book, Integer> borrowedBookCount;
    public static volatile SingularAttribute<Book, Integer> quantity;
    public static volatile ListAttribute<Book, Barcode> barcodeList;
    public static volatile SingularAttribute<Book, String> bookName;

}