package com.lms;

import com.lms.Book;
import com.lms.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(ReserveBook.class)
public class ReserveBook_ { 

    public static volatile SingularAttribute<ReserveBook, Date> reserveFinishDate;
    public static volatile SingularAttribute<ReserveBook, Integer> reserveBookId;
    public static volatile SingularAttribute<ReserveBook, Date> reserveStartDate;
    public static volatile SingularAttribute<ReserveBook, User> userId;
    public static volatile SingularAttribute<ReserveBook, Book> bookId;

}