package com.lms;

import com.lms.BorrowBook;
import com.lms.Feedback;
import com.lms.Fines;
import com.lms.Groups;
import com.lms.RequestNewBook;
import com.lms.ReserveBook;
import com.lms.ReturnBook;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, Integer> barcodeId;
    public static volatile ListAttribute<User, ReserveBook> reserveBookList;
    public static volatile SingularAttribute<User, Boolean> status;
    public static volatile ListAttribute<User, ReturnBook> returnBookList;
    public static volatile ListAttribute<User, RequestNewBook> requestNewBookList;
    public static volatile SingularAttribute<User, String> theme;
    public static volatile SingularAttribute<User, byte[]> image;
    public static volatile ListAttribute<User, Fines> finesList;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, String> contact;
    public static volatile ListAttribute<User, BorrowBook> borrowBookList;
    public static volatile SingularAttribute<User, Groups> groupId;
    public static volatile SingularAttribute<User, String> username;
    public static volatile SingularAttribute<User, String> address;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, String> name;
    public static volatile SingularAttribute<User, Integer> userId;
    public static volatile ListAttribute<User, Feedback> feedbackList;

}