package com.lms;

import com.lms.Book;
import com.lms.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(ReturnBook.class)
public class ReturnBook_ { 

    public static volatile SingularAttribute<ReturnBook, User> userId;
    public static volatile SingularAttribute<ReturnBook, Book> bookId;
    public static volatile SingularAttribute<ReturnBook, Integer> returnBookId;
    public static volatile SingularAttribute<ReturnBook, Date> returnDate;

}