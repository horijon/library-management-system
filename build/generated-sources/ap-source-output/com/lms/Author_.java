package com.lms;

import com.lms.Book;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(Author.class)
public class Author_ { 

    public static volatile ListAttribute<Author, Book> bookList;
    public static volatile SingularAttribute<Author, String> authorName;
    public static volatile SingularAttribute<Author, Integer> authorId;

}