package com.lms;

import com.lms.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(Feedback.class)
public class Feedback_ { 

    public static volatile SingularAttribute<Feedback, String> description;
    public static volatile SingularAttribute<Feedback, String> subject;
    public static volatile SingularAttribute<Feedback, User> userId;
    public static volatile SingularAttribute<Feedback, Integer> feedbackId;
    public static volatile SingularAttribute<Feedback, Date> date;

}