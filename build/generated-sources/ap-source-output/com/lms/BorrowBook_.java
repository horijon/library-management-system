package com.lms;

import com.lms.Book;
import com.lms.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-09-11T08:53:49")
@StaticMetamodel(BorrowBook.class)
public class BorrowBook_ { 

    public static volatile SingularAttribute<BorrowBook, Integer> borrowBookId;
    public static volatile SingularAttribute<BorrowBook, Date> expiryDate;
    public static volatile SingularAttribute<BorrowBook, User> userId;
    public static volatile SingularAttribute<BorrowBook, Book> bookId;
    public static volatile SingularAttribute<BorrowBook, Date> borrowDate;

}