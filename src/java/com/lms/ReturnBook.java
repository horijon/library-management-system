/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kshitij
 */
@Entity
@Table(name = "return_book", catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReturnBook.findAll", query = "SELECT r FROM ReturnBook r"),
    @NamedQuery(name = "ReturnBook.findByReturnBookId", query = "SELECT r FROM ReturnBook r WHERE r.returnBookId = :returnBookId"),
    @NamedQuery(name = "ReturnBook.findByReturnDate", query = "SELECT r FROM ReturnBook r WHERE r.returnDate = :returnDate")})
public class ReturnBook implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "return_book_id", nullable = false)
    private Integer returnBookId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "return_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date returnDate;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;
    @JoinColumn(name = "book_id", referencedColumnName = "book_id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Book bookId;

    public ReturnBook() {
    }

    public ReturnBook(Integer returnBookId) {
        this.returnBookId = returnBookId;
    }

    public ReturnBook(Integer returnBookId, Date returnDate) {
        this.returnBookId = returnBookId;
        this.returnDate = returnDate;
    }

    public Integer getReturnBookId() {
        return returnBookId;
    }

    public void setReturnBookId(Integer returnBookId) {
        this.returnBookId = returnBookId;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Book getBookId() {
        return bookId;
    }

    public void setBookId(Book bookId) {
        this.bookId = bookId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (returnBookId != null ? returnBookId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReturnBook)) {
            return false;
        }
        ReturnBook other = (ReturnBook) object;
        if ((this.returnBookId == null && other.returnBookId != null) || (this.returnBookId != null && !this.returnBookId.equals(other.returnBookId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.ReturnBook[ returnBookId=" + returnBookId + " ]";
    }
    
}
