package com.lms;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.MeterGaugeChartModel;
import org.primefaces.model.chart.PieChartModel;

@ManagedBean
public class ChartBean implements Serializable {

    private BarChartModel barModel;
    private PieChartModel pieModel2;
    private MeterGaugeChartModel meterGaugeModel1;
    private MeterGaugeChartModel meterGaugeModel2;
    private int count;
    private int reserve_count;
    private int borrow_count;
    private int issued_count;
    private int countfive;
    
	public ChartBean() {
        createPieModels();
        createMeterGaugeModels();
        createBarModels();
	}

	public void itemSelect(ItemSelectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
                        "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());
        
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
    
    public PieChartModel getPieModel2() {
        return pieModel2;
    }
     
    public MeterGaugeChartModel getMeterGaugeModel1() {
        return meterGaugeModel1;
    }

    public BarChartModel getBarModel() {
        return barModel;
    }
    
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();

        ChartSeries boys = new ChartSeries();
        boys.setLabel("Boys");
        boys.set("2004", 120);
        boys.set("2005", 100);
        boys.set("2006", 44);
        boys.set("2007", 150);
        boys.set("2008", 25);

        ChartSeries girls = new ChartSeries();
        girls.setLabel("Girls");
        girls.set("2004", 52);
        girls.set("2005", 60);
        girls.set("2006", 110);
        girls.set("2007", 135);
        girls.set("2008", 120);

        model.addSeries(boys);
        model.addSeries(girls);
        
        return model;
    }
    
    private void createBarModels() {
        createBarModel();
    }
    
    private void createBarModel() {
        barModel = initBarModel();
        
        barModel.setTitle("Bar Chart");
        barModel.setLegendPosition("ne");
        
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Gender");
        
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Births");
        yAxis.setMin(0);
        yAxis.setMax(200);
    }
        
    private void createPieModels() {
        createPieModel2();
    }

    private void createPieModel2() {
        
        
        Connection con = null;
        PreparedStatement ps_book_info = null;
        PreparedStatement ps_reserve_book_info = null;
        PreparedStatement ps_borrow_book_info = null;
        
        try {
            con = DataConnect.getConnection();
            ps_book_info = con.prepareStatement("SELECT quantity FROM profile.book");
            ps_reserve_book_info = con.prepareStatement("SELECT count(*) as reservecount FROM profile.reserve_book");
            ps_borrow_book_info = con.prepareStatement("SELECT count(*) as borrowcount FROM profile.borrow_book");
            ResultSet rs_book_info = ps_book_info.executeQuery();
            ResultSet rs_reserve_book_info = ps_reserve_book_info.executeQuery();
            ResultSet rs_borrow_book_info = ps_borrow_book_info.executeQuery();
            count=0;
            while(rs_book_info.next()) {
                count = count + rs_book_info.getInt("quantity");
            }
            
            if(rs_reserve_book_info.next()) {
                reserve_count = rs_reserve_book_info.getInt("reservecount");
            }
            
            if(rs_borrow_book_info.next()) {
                borrow_count = rs_borrow_book_info.getInt("borrowcount");
            }
                
        } catch (SQLException ex) {
            } finally {
           
        }
        
        
        
        
        
        pieModel2 = new PieChartModel();
        System.out.println("count = " + count);
        System.out.println("reserve = " + reserve_count);
        System.out.println("borrow = " + borrow_count);
        pieModel2.set("Available", count);
        pieModel2.set("Reserved", reserve_count);
        pieModel2.set("Borrowed", 20);
        pieModel2.setSeriesColors("2288AA,cc6666,E7E658");
        //66cc66,93b75f,E7E658,cc6666
        
        pieModel2.setTitle("Custom Pie");
        pieModel2.setLegendPosition("e");
        pieModel2.setFill(false);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDiameter(150);
    }
    
    private MeterGaugeChartModel initMeterGaugeModel() {
        Connection con = null;
        PreparedStatement ps_book_info = null;
        PreparedStatement ps_reserve_book_info = null;
        PreparedStatement ps_borrow_book_info = null;
        
        try {
            con = DataConnect.getConnection();
            ps_book_info = con.prepareStatement("SELECT quantity FROM profile.book");
            ps_reserve_book_info = con.prepareStatement("SELECT count(*) as reservecount FROM profile.reserve_book");
            ps_borrow_book_info = con.prepareStatement("SELECT count(*) as borrowcount FROM profile.borrow_book");
            ResultSet rs_book_info = ps_book_info.executeQuery();
            ResultSet rs_reserve_book_info = ps_reserve_book_info.executeQuery();
            ResultSet rs_borrow_book_info = ps_borrow_book_info.executeQuery();
            count=0;
            while(rs_book_info.next()) {
                count = count + rs_book_info.getInt("quantity");
            }
            
            if(rs_reserve_book_info.next()) {
                reserve_count = rs_reserve_book_info.getInt("reservecount");
            }
            
            if(rs_borrow_book_info.next()) {
                borrow_count = rs_borrow_book_info.getInt("borrowcount");
            }
                
                issued_count = reserve_count + borrow_count;
                countfive = count % 5;
                count = count - countfive;
                
        } catch (SQLException ex) {
            } finally {
           
        }
        
        List<Number> intervals = new ArrayList<Number>(){{
            add(10);
            add(20);
            add(30);
            add(40);
            add(count);
        }};
        
        return new MeterGaugeChartModel(issued_count, intervals);
    }
    
    private void createMeterGaugeModels() {
        meterGaugeModel1 = initMeterGaugeModel();
        meterGaugeModel1.setTitle("MeterGauge Chart");
        meterGaugeModel1.setGaugeLabel("issued/total");
        meterGaugeModel1.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
        meterGaugeModel1.setGaugeLabelPosition("bottom");
        
        meterGaugeModel2 = initMeterGaugeModel();
        meterGaugeModel2.setTitle("Custom Options");
        meterGaugeModel2.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
        meterGaugeModel2.setGaugeLabel("km/h");
        meterGaugeModel2.setGaugeLabelPosition("bottom");
        meterGaugeModel2.setShowTickLabels(false);
        meterGaugeModel2.setLabelHeightAdjust(110);
        meterGaugeModel2.setIntervalOuterRadius(130);
    }

}