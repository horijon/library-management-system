/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kshitij
 */
@Entity
@Table(catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByUserId", query = "SELECT u FROM User u WHERE u.userId = :userId"),
    @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
    @NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password"),
    @NamedQuery(name = "User.findByContact", query = "SELECT u FROM User u WHERE u.contact = :contact"),
    @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
    @NamedQuery(name = "User.findByAddress", query = "SELECT u FROM User u WHERE u.address = :address"),
    @NamedQuery(name = "User.findByName", query = "SELECT u FROM User u WHERE u.name = :name"),
    @NamedQuery(name = "User.findByStatus", query = "SELECT u FROM User u WHERE u.status = :status"),
    @NamedQuery(name = "User.findByTheme", query = "SELECT u FROM User u WHERE u.theme = :theme"),
    @NamedQuery(name = "User.findByBarcodeId", query = "SELECT u FROM User u WHERE u.barcodeId = :barcodeId")})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_id", nullable = false)
    private Integer userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String password;
    @Size(max = 2147483647)
    @Column(length = 2147483647)
    private String contact;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 2147483647)
    @Column(length = 2147483647)
    private String email;
    @Size(max = 255)
    @Column(length = 255)
    private String address;
    @Lob
    private byte[] image;
    @Size(max = 255)
    @Column(length = 255)
    private String name;
    private Boolean status;
    @Size(max = 255)
    @Column(length = 255)
    private String theme;
    @Column(name = "barcode_id")
    private Integer barcodeId;
    @OneToMany(mappedBy = "userId", fetch = FetchType.LAZY)
    private List<Feedback> feedbackList;
    @OneToMany(mappedBy = "userId", fetch = FetchType.LAZY)
    private List<ReserveBook> reserveBookList;
    @OneToMany(mappedBy = "userId", fetch = FetchType.LAZY)
    private List<RequestNewBook> requestNewBookList;
    @OneToMany(mappedBy = "userId", fetch = FetchType.LAZY)
    private List<Fines> finesList;
    @OneToMany(mappedBy = "userId", fetch = FetchType.LAZY)
    private List<ReturnBook> returnBookList;
    @OneToMany(mappedBy = "userId", fetch = FetchType.LAZY)
    private List<BorrowBook> borrowBookList;
    @JoinColumn(name = "group_id", referencedColumnName = "group_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Groups groupId;

    public User() {
    }

    public User(Integer userId) {
        this.userId = userId;
    }

    public User(Integer userId, String username, String password) {
        this.userId = userId;
        this.username = username;
        this.password = password;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Integer getBarcodeId() {
        return barcodeId;
    }

    public void setBarcodeId(Integer barcodeId) {
        this.barcodeId = barcodeId;
    }

    @XmlTransient
    public List<Feedback> getFeedbackList() {
        return feedbackList;
    }

    public void setFeedbackList(List<Feedback> feedbackList) {
        this.feedbackList = feedbackList;
    }

    @XmlTransient
    public List<ReserveBook> getReserveBookList() {
        return reserveBookList;
    }

    public void setReserveBookList(List<ReserveBook> reserveBookList) {
        this.reserveBookList = reserveBookList;
    }

    @XmlTransient
    public List<RequestNewBook> getRequestNewBookList() {
        return requestNewBookList;
    }

    public void setRequestNewBookList(List<RequestNewBook> requestNewBookList) {
        this.requestNewBookList = requestNewBookList;
    }

    @XmlTransient
    public List<Fines> getFinesList() {
        return finesList;
    }

    public void setFinesList(List<Fines> finesList) {
        this.finesList = finesList;
    }

    @XmlTransient
    public List<ReturnBook> getReturnBookList() {
        return returnBookList;
    }

    public void setReturnBookList(List<ReturnBook> returnBookList) {
        this.returnBookList = returnBookList;
    }

    @XmlTransient
    public List<BorrowBook> getBorrowBookList() {
        return borrowBookList;
    }

    public void setBorrowBookList(List<BorrowBook> borrowBookList) {
        this.borrowBookList = borrowBookList;
    }

    public Groups getGroupId() {
        return groupId;
    }

    public void setGroupId(Groups groupId) {
        this.groupId = groupId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.User[ userId=" + userId + " ]";
    }
    
    public String login() {
        //System.out.println("Username : " + username + " and password : " + l.hashPasswordSHABase64(password));
        Logger.getLogger(User.class.getName()).log(Level.SEVERE, Login.hashPasswordSHABase64(password));
        
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;

        try {
            con = DataConnect.getConnection();
            ps = con.prepareStatement("SELECT 1 FROM profile.user u WHERE u.username = ? AND u.password = ?");
            //ps is prepareStatement
            ps1 = con.prepareStatement("INSERT INTO profile.user_history(username) VALUES (?)");
            ps1.setString(1, username);
            ps.setString(1, username);
            ps.setString(2, password);
            ps1.execute();
            ResultSet rs = ps.executeQuery();

            if (rs.next() == true) {
                HttpSession session = SessionBean.getSession();
                session.setAttribute("username", username);
                return "home?facesRedirect=true";
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage("username", new FacesMessage("Invalid UserName and Password"));
            }
        } catch (SQLException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("username", new FacesMessage("Invalid UserName and Password"));
            return "home?facesRedirect=true";
        } finally {
            DataConnect.close(con);
        }
        return "login?facesRedirect=true";
    }

    public String logout() {
        HttpSession session = SessionBean.getSession();
        session.invalidate();
        return "logout";
    }
    
}
