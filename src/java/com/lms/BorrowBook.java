/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kshitij
 */
@Entity
@Table(name = "borrow_book", catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BorrowBook.findAll", query = "SELECT b FROM BorrowBook b"),
    @NamedQuery(name = "BorrowBook.findByBorrowBookId", query = "SELECT b FROM BorrowBook b WHERE b.borrowBookId = :borrowBookId"),
    @NamedQuery(name = "BorrowBook.findByBorrowDate", query = "SELECT b FROM BorrowBook b WHERE b.borrowDate = :borrowDate"),
    @NamedQuery(name = "BorrowBook.findByExpiryDate", query = "SELECT b FROM BorrowBook b WHERE b.expiryDate = :expiryDate")})
public class BorrowBook implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "borrow_book_id", nullable = false)
    private Integer borrowBookId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "borrow_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date borrowDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "expiry_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date expiryDate;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;
    @JoinColumn(name = "book_id", referencedColumnName = "book_id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Book bookId;

    public BorrowBook() {
    }

    public BorrowBook(Integer borrowBookId) {
        this.borrowBookId = borrowBookId;
    }

    public BorrowBook(Integer borrowBookId, Date borrowDate, Date expiryDate) {
        this.borrowBookId = borrowBookId;
        this.borrowDate = borrowDate;
        this.expiryDate = expiryDate;
    }

    public Integer getBorrowBookId() {
        return borrowBookId;
    }

    public void setBorrowBookId(Integer borrowBookId) {
        this.borrowBookId = borrowBookId;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Book getBookId() {
        return bookId;
    }

    public void setBookId(Book bookId) {
        this.bookId = bookId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (borrowBookId != null ? borrowBookId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BorrowBook)) {
            return false;
        }
        BorrowBook other = (BorrowBook) object;
        if ((this.borrowBookId == null && other.borrowBookId != null) || (this.borrowBookId != null && !this.borrowBookId.equals(other.borrowBookId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.BorrowBook[ borrowBookId=" + borrowBookId + " ]";
    }
    
}
