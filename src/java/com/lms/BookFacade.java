/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kshitij
 */
@Stateless
public class BookFacade extends AbstractFacade<Book> {
    @PersistenceContext(unitName = "lmsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BookFacade() {
        super(Book.class);
    }
    
    public void reserve(Book b) {
        em.getEntityManagerFactory().getCache().evictAll();
        Connection con = null;
        //ps is prepared statement
        PreparedStatement ps_user_info = null;
        PreparedStatement ps_book_info = null;
        PreparedStatement ps_update_book = null;
        PreparedStatement ps_reserve = null;

        try {
            con = DataConnect.getConnection();
            //user info start
            ps_user_info = con.prepareStatement("SELECT user_id from profile.user where username = ?");
            HttpSession session = SessionBean.getSession();
            String username = (String) session.getAttribute("username");
            ps_user_info.setString(1, username);
            ResultSet rs_userid = ps_user_info.executeQuery();
            int user_id = 0;
            if (rs_userid.next()) {
                user_id = rs_userid.getInt("user_id");
            }
            //user info end
            //book info start
            ps_book_info = con.prepareStatement("SELECT quantity, reserve_book_count, borrowed_book_count from profile.book where book_id = ?");
            ps_book_info.setInt(1, b.getBookId());
            ResultSet rs_book_info = ps_book_info.executeQuery();
            //book info end
            //update book preparation start
            ps_update_book = con.prepareStatement("UPDATE profile.book set reserve_book_count = COALESCE(reserve_book_count,0) + 1 where book_id = ?");
            ps_update_book.setInt(1, b.getBookId());
            //update book preparation end
            //insert reserve book preparation start
            ps_reserve = con.prepareStatement("INSERT INTO profile.reserve_book(book_id, reserve_finish_date, user_id) VALUES (?, now() + interval '3 days', ?)");
            ps_reserve.setInt(1, b.getBookId());
            ps_reserve.setInt(2, user_id);
            //insert reserve book preparation end
            //program flow start
            int quantity, reserve_book_count, borrowed_book_count;
            if (rs_book_info.next()) {
                quantity = rs_book_info.getInt("quantity");
                reserve_book_count = rs_book_info.getInt("reserve_book_count");
                borrowed_book_count = rs_book_info.getInt("borrowed_book_count");
                int b_r = reserve_book_count + borrowed_book_count;
                System.out.println("Total : " + b_r + "quantity : " + quantity);
                if (quantity > b_r) {
                    System.out.println("reservation possible");
                    try {
                        ps_reserve.executeUpdate();
                        int executeUpdate;
                        executeUpdate = ps_update_book.executeUpdate();
                    } catch (Exception ex) {
                        System.out.println("Some exception has occured" + ex);
                    }
                    System.out.println("I am executed insert");

                } else {
                    System.out.println("insert did not execute");
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage("error msg", new FacesMessage("Reservation already full"));
                }

            }
        } catch (SQLException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("Book reservation", new FacesMessage("DB error"));

        } finally {
            DataConnect.close(con);
            em.getEntityManagerFactory().getCache().evictAll();
        }
    }

    public boolean disable(Book b) {

        Connection con = null;
        //ps is prepared statement
        PreparedStatement ps_check = null;
        try {
            con = DataConnect.getConnection();
            //user info start
            ps_check = con.prepareStatement("SELECT quantity, reserve_book_count, borrowed_book_count from profile.book where book_id = ?");
            ps_check.setInt(1, b.getBookId());
            ResultSet rs_check_info = ps_check.executeQuery();
            int quantity, reserve_book_count, borrowed_book_count;
            if (rs_check_info.next()) {
                quantity = rs_check_info.getInt("quantity");
                reserve_book_count = rs_check_info.getInt("reserve_book_count");
                borrowed_book_count = rs_check_info.getInt("borrowed_book_count");
                int b_r = reserve_book_count + borrowed_book_count;
                System.out.println("Test : total = " + b_r + " quantity = " + quantity);
                if (quantity == b_r) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("Book reservation", new FacesMessage("DB error"));

        } finally {
            DataConnect.close(con);
        }
        return false;
    }
    
}
