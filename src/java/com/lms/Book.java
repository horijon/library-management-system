/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kshitij
 */
@Entity
@Table(catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Book.findAll", query = "SELECT b FROM Book b"),
    @NamedQuery(name = "Book.findByBookId", query = "SELECT b FROM Book b WHERE b.bookId = :bookId"),
    @NamedQuery(name = "Book.findByBookName", query = "SELECT b FROM Book b WHERE b.bookName = :bookName"),
    @NamedQuery(name = "Book.findByBookEdition", query = "SELECT b FROM Book b WHERE b.bookEdition = :bookEdition"),
    @NamedQuery(name = "Book.findByBookNumber", query = "SELECT b FROM Book b WHERE b.bookNumber = :bookNumber"),
    @NamedQuery(name = "Book.findByPublisher", query = "SELECT b FROM Book b WHERE b.publisher = :publisher"),
    @NamedQuery(name = "Book.findByQuantity", query = "SELECT b FROM Book b WHERE b.quantity = :quantity"),
    @NamedQuery(name = "Book.findByBookStatus", query = "SELECT b FROM Book b WHERE b.bookStatus = :bookStatus"),
    @NamedQuery(name = "Book.findByBorrowedBookCount", query = "SELECT b FROM Book b WHERE b.borrowedBookCount = :borrowedBookCount"),
    @NamedQuery(name = "Book.findByReserveBookCount", query = "SELECT b FROM Book b WHERE b.reserveBookCount = :reserveBookCount"),
    @NamedQuery(name = "Book.findByBarcodeId", query = "SELECT b FROM Book b WHERE b.barcodeId = :barcodeId")})
public class Book implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "book_id", nullable = false)
    private Integer bookId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "book_name", nullable = false, length = 255)
    private String bookName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "book_edition", nullable = false)
    private int bookEdition;
    @Size(max = 255)
    @Column(name = "book_number", length = 255)
    private String bookNumber;
    @Size(max = 255)
    @Column(length = 255)
    private String publisher;
    private Integer quantity;
    @Size(max = 255)
    @Column(name = "book_status", length = 255)
    private String bookStatus;
    @Column(name = "borrowed_book_count")
    private Integer borrowedBookCount;
    @Column(name = "reserve_book_count")
    private Integer reserveBookCount;
    @Size(max = 255)
    @Column(name = "barcode_id", length = 255)
    private String barcodeId;
     @OneToMany(cascade = CascadeType.ALL, mappedBy = "bookId", fetch = FetchType.LAZY)
    private List<ReserveBook> reserveBookList;
    @OneToMany(mappedBy = "bookId", fetch = FetchType.LAZY)
    private List<Fines> finesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bookId", fetch = FetchType.LAZY)
    private List<ReturnBook> returnBookList;
    @JoinColumn(name = "author_id", referencedColumnName = "author_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Author authorId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bookId", fetch = FetchType.LAZY)
    private List<BorrowBook> borrowBookList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bookId", fetch = FetchType.LAZY)
    private List<Barcode> barcodeList;

    public Book() {
    }

    public Book(Integer bookId) {
        this.bookId = bookId;
    }

    public Book(Integer bookId, String bookName, int bookEdition) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookEdition = bookEdition;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getBookEdition() {
        return bookEdition;
    }

    public void setBookEdition(int bookEdition) {
        this.bookEdition = bookEdition;
    }

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getBookStatus() {
        return bookStatus;
    }

    public void setBookStatus(String bookStatus) {
        this.bookStatus = bookStatus;
    }

    public Integer getBorrowedBookCount() {
        return borrowedBookCount;
    }

    public void setBorrowedBookCount(Integer borrowedBookCount) {
        this.borrowedBookCount = borrowedBookCount;
    }

    public Integer getReserveBookCount() {
        return reserveBookCount;
    }

    public void setReserveBookCount(Integer reserveBookCount) {
        this.reserveBookCount = reserveBookCount;
    }

    public String getBarcodeId() {
        return barcodeId;
    }

    public void setBarcodeId(String barcodeId) {
        this.barcodeId = barcodeId;
    }
    
        @XmlTransient
    public List<ReserveBook> getReserveBookList() {
        return reserveBookList;
    }

    public void setReserveBookList(List<ReserveBook> reserveBookList) {
        this.reserveBookList = reserveBookList;
    }

    @XmlTransient
    public List<Fines> getFinesList() {
        return finesList;
    }

    public void setFinesList(List<Fines> finesList) {
        this.finesList = finesList;
    }

    @XmlTransient
    public List<ReturnBook> getReturnBookList() {
        return returnBookList;
    }

    public void setReturnBookList(List<ReturnBook> returnBookList) {
        this.returnBookList = returnBookList;
    }

    public Author getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Author authorId) {
        this.authorId = authorId;
    }

    @XmlTransient
    public List<BorrowBook> getBorrowBookList() {
        return borrowBookList;
    }

    public void setBorrowBookList(List<BorrowBook> borrowBookList) {
        this.borrowBookList = borrowBookList;
    }

    @XmlTransient
    public List<Barcode> getBarcodeList() {
        return barcodeList;
    }

    public void setBarcodeList(List<Barcode> barcodeList) {
        this.barcodeList = barcodeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookId != null ? bookId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Book)) {
            return false;
        }
        Book other = (Book) object;
        if ((this.bookId == null && other.bookId != null) || (this.bookId != null && !this.bookId.equals(other.bookId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.Book[ bookId=" + bookId + " ]";
    }
    
}
