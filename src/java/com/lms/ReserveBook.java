/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kshitij
 */
@Entity
@Table(name = "reserve_book", catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReserveBook.findAll", query = "SELECT r FROM ReserveBook r"),
    @NamedQuery(name = "ReserveBook.findByUserId", query = "SELECT r FROM ReserveBook r WHERE r.userId = :userId"),
    @NamedQuery(name = "ReserveBook.findByReserveBookId", query = "SELECT r FROM ReserveBook r WHERE r.reserveBookId = :reserveBookId"),
    @NamedQuery(name = "ReserveBook.findByReserveStartDate", query = "SELECT r FROM ReserveBook r WHERE r.reserveStartDate = :reserveStartDate"),
    @NamedQuery(name = "ReserveBook.findByReserveFinishDate", query = "SELECT r FROM ReserveBook r WHERE r.reserveFinishDate = :reserveFinishDate")})
public class ReserveBook implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "reserve_book_id", nullable = false)
    private Integer reserveBookId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reserve_start_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date reserveStartDate;
    @Column(name = "reserve_finish_date")
    @Temporal(TemporalType.DATE)
    private Date reserveFinishDate;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;
    @JoinColumn(name = "book_id", referencedColumnName = "book_id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Book bookId;

    public ReserveBook() {
    }

    public ReserveBook(Integer reserveBookId) {
        this.reserveBookId = reserveBookId;
    }

    public ReserveBook(Integer reserveBookId, Date reserveStartDate) {
        this.reserveBookId = reserveBookId;
        this.reserveStartDate = reserveStartDate;
    }

    public Integer getReserveBookId() {
        return reserveBookId;
    }

    public void setReserveBookId(Integer reserveBookId) {
        this.reserveBookId = reserveBookId;
    }

    public Date getReserveStartDate() {
        return reserveStartDate;
    }

    public void setReserveStartDate(Date reserveStartDate) {
        this.reserveStartDate = reserveStartDate;
    }

    public Date getReserveFinishDate() {
        return reserveFinishDate;
    }

    public void setReserveFinishDate(Date reserveFinishDate) {
        this.reserveFinishDate = reserveFinishDate;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Book getBookId() {
        return bookId;
    }

    public void setBookId(Book bookId) {
        this.bookId = bookId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reserveBookId != null ? reserveBookId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReserveBook)) {
            return false;
        }
        ReserveBook other = (ReserveBook) object;
        if ((this.reserveBookId == null && other.reserveBookId != null) || (this.reserveBookId != null && !this.reserveBookId.equals(other.reserveBookId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.ReserveBook[ reserveBookId=" + reserveBookId + " ]";
    }
    
}
