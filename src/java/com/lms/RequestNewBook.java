/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kshitij
 */
@Entity
@Table(name = "request_new_book", catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RequestNewBook.findAll", query = "SELECT r FROM RequestNewBook r"),
    @NamedQuery(name = "RequestNewBook.findByRequestNewBookId", query = "SELECT r FROM RequestNewBook r WHERE r.requestNewBookId = :requestNewBookId"),
    @NamedQuery(name = "RequestNewBook.findByBookName", query = "SELECT r FROM RequestNewBook r WHERE r.bookName = :bookName"),
    @NamedQuery(name = "RequestNewBook.findByAuthor", query = "SELECT r FROM RequestNewBook r WHERE r.author = :author"),
    @NamedQuery(name = "RequestNewBook.findByPublisher", query = "SELECT r FROM RequestNewBook r WHERE r.publisher = :publisher"),
    @NamedQuery(name = "RequestNewBook.findByEdition", query = "SELECT r FROM RequestNewBook r WHERE r.edition = :edition")})
public class RequestNewBook implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "request_new_book_id", nullable = false)
    private Integer requestNewBookId;
    @Size(max = 255)
    @Column(name = "book_name", length = 255)
    private String bookName;
    @Size(max = 255)
    @Column(length = 255)
    private String author;
    @Size(max = 255)
    @Column(length = 255)
    private String publisher;
    private Integer edition;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;

    public RequestNewBook() {
    }

    public RequestNewBook(Integer requestNewBookId) {
        this.requestNewBookId = requestNewBookId;
    }

    public Integer getRequestNewBookId() {
        return requestNewBookId;
    }

    public void setRequestNewBookId(Integer requestNewBookId) {
        this.requestNewBookId = requestNewBookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Integer getEdition() {
        return edition;
    }

    public void setEdition(Integer edition) {
        this.edition = edition;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (requestNewBookId != null ? requestNewBookId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequestNewBook)) {
            return false;
        }
        RequestNewBook other = (RequestNewBook) object;
        if ((this.requestNewBookId == null && other.requestNewBookId != null) || (this.requestNewBookId != null && !this.requestNewBookId.equals(other.requestNewBookId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.RequestNewBook[ requestNewBookId=" + requestNewBookId + " ]";
    }
    
}
