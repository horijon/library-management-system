/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kshitij
 */
@Stateless
public class ReturnBookFacade extends AbstractFacade<ReturnBook> {
    @PersistenceContext(unitName = "lmsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReturnBookFacade() {
        super(ReturnBook.class);
    }
    
}
