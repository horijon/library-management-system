/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kshitij
 */
@Stateless
public class BorrowBookFacade extends AbstractFacade<BorrowBook> {
    @PersistenceContext(unitName = "lmsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BorrowBookFacade() {
        super(BorrowBook.class);
    }
    
}
