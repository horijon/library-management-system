/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kshitij
 */
@Entity
@Table(name = "user_history", catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserHistory.findAll", query = "SELECT u FROM UserHistory u"),
    @NamedQuery(name = "UserHistory.findByUserHistoryId", query = "SELECT u FROM UserHistory u WHERE u.userHistoryId = :userHistoryId"),
    @NamedQuery(name = "UserHistory.findByUsername", query = "SELECT u FROM UserHistory u WHERE u.username = :username"),
    @NamedQuery(name = "UserHistory.findByTimeStamp", query = "SELECT u FROM UserHistory u WHERE u.timeStamp = :timeStamp")})
public class UserHistory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_history_id", nullable = false)
    private Integer userHistoryId;
    @Size(max = 2147483647)
    @Column(length = 2147483647)
    private String username;
    @Column(name = "time_stamp")
    @Temporal(TemporalType.DATE)
    private Date timeStamp;

    public UserHistory() {
    }

    public UserHistory(Integer userHistoryId) {
        this.userHistoryId = userHistoryId;
    }

    public Integer getUserHistoryId() {
        return userHistoryId;
    }

    public void setUserHistoryId(Integer userHistoryId) {
        this.userHistoryId = userHistoryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userHistoryId != null ? userHistoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHistory)) {
            return false;
        }
        UserHistory other = (UserHistory) object;
        if ((this.userHistoryId == null && other.userHistoryId != null) || (this.userHistoryId != null && !this.userHistoryId.equals(other.userHistoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.UserHistory[ userHistoryId=" + userHistoryId + " ]";
    }
    
}
