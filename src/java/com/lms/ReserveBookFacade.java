/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author kshitij
 */
@Stateless
public class ReserveBookFacade extends AbstractFacade<ReserveBook> {
    @PersistenceContext(unitName = "lmsPU")
    private EntityManager em;
    
    private List<ReserveBook> reserves = null;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReserveBookFacade() {
        super(ReserveBook.class);
    }
    
    public List<ReserveBook> findbyUserId(Integer userid) {
        try{
        Query query = em.createNamedQuery("ReserveBook.findByUserId");
        query.setParameter("userId", userid);
        reserves = query.getResultList();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return reserves;
    }
    
}
