/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kshitij
 */
@Entity
@Table(catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Barcode.findAll", query = "SELECT b FROM Barcode b"),
    @NamedQuery(name = "Barcode.findByBarcodeId", query = "SELECT b FROM Barcode b WHERE b.barcodeId = :barcodeId")})
public class Barcode implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "barcode_id", nullable = false)
    private Integer barcodeId;
    @JoinColumn(name = "book_id", referencedColumnName = "book_id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Book bookId;

    public Barcode() {
    }

    public Barcode(Integer barcodeId) {
        this.barcodeId = barcodeId;
    }

    public Integer getBarcodeId() {
        return barcodeId;
    }

    public void setBarcodeId(Integer barcodeId) {
        this.barcodeId = barcodeId;
    }

    public Book getBookId() {
        return bookId;
    }

    public void setBookId(Book bookId) {
        this.bookId = bookId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (barcodeId != null ? barcodeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Barcode)) {
            return false;
        }
        Barcode other = (Barcode) object;
        if ((this.barcodeId == null && other.barcodeId != null) || (this.barcodeId != null && !this.barcodeId.equals(other.barcodeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.Barcode[ barcodeId=" + barcodeId + " ]";
    }
    
}
