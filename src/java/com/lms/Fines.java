/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kshitij
 */
@Entity
@Table(catalog = "lms", schema = "profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fines.findAll", query = "SELECT f FROM Fines f"),
    @NamedQuery(name = "Fines.findByFineId", query = "SELECT f FROM Fines f WHERE f.fineId = :fineId"),
    @NamedQuery(name = "Fines.findByFineamount", query = "SELECT f FROM Fines f WHERE f.fineamount = :fineamount")})
public class Fines implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "fine_id", nullable = false)
    private Integer fineId;
    private Integer fineamount;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;
    @JoinColumn(name = "book_id", referencedColumnName = "book_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Book bookId;

    public Fines() {
    }

    public Fines(Integer fineId) {
        this.fineId = fineId;
    }

    public Integer getFineId() {
        return fineId;
    }

    public void setFineId(Integer fineId) {
        this.fineId = fineId;
    }

    public Integer getFineamount() {
        return fineamount;
    }

    public void setFineamount(Integer fineamount) {
        this.fineamount = fineamount;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Book getBookId() {
        return bookId;
    }

    public void setBookId(Book bookId) {
        this.bookId = bookId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fineId != null ? fineId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fines)) {
            return false;
        }
        Fines other = (Fines) object;
        if ((this.fineId == null && other.fineId != null) || (this.fineId != null && !this.fineId.equals(other.fineId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lms.Fines[ fineId=" + fineId + " ]";
    }
    
}
