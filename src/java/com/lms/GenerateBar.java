/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import com.aspose.barcode.*;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author kshitij
 */
@ManagedBean(name = "generateBar")
@SessionScoped
public class GenerateBar extends Applet {

    private String BarCodeText;
    
    public GenerateBar() {
        System.out.println("Entered GenerateBar Constructor");
        this.BarCodeText = "987";
        repaint();
    }
    
    //public GenerateBar (String BarCodeText) {
    //    this.BarCodeText = BarCodeText;
    //    System.out.println("BarCodeText: " + BarCodeText);
    //    repaint();
    //    System.out.println("After repaint");
    //}

    @Override
     public void paint(Graphics g)
        {
         System.out.println("Entered here first");
         super.paint(g);
         
         System.out.println("Entered here");
         //String BarCodeText = "123";
         String BarCodeImageFolder = "e:\\lms\\BarCodes\\";
         // Instantiate a BarCodeBuilder
         BarCodeBuilder b = new BarCodeBuilder();

         // Choose Symbology to be Pdf417
         b.setSymbologyType(Symbology.Code39Standard);

         // Small module's width to be 1 millimeter
         b.setxDimension(1);

         // Small module's height to be 4 millimeter
         b.setyDimension(4);

         // Text to be encoded
         b.setCodeText(BarCodeText);

         // Set up the size of marginal areas
         b.setMargins(new MarginsF(10, 10, 10, 10));

           // Save the barcode to disk
           //b.save(getCodeBase().getPath() + "barcode.png");
           //System.out.println("Saved barcode image to " + getCodeBase().getPath() + "barcode.png");

           b.save(BarCodeImageFolder + BarCodeText + ".gif");

           // Load and Draw the image on applet
           MediaTracker tr = new MediaTracker(this);
           Image img = getImage(getCodeBase(), BarCodeText + ".gif");
           tr.addImage(img,0);
           g.drawImage(img, 0, 0, this);

        }
     public void go(){
         
     }
}
