/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lms;
import java.applet.Applet;
import java.awt.*;
import com.aspose.barcoderecognition.*;

/**
 *
 * @author kshitij
 */
public class ScanBar extends Applet{
     public void paint(Graphics g)
    {
        try
        {
            //Get the image we created, or any other image available
            String img = ("e:\\lms\\BarCodes\\123.gif");

            //Instantiate a BarCodeReader
            BarCodeReader r = new BarCodeReader(img, BarCodeReadType.Code39Standard);
            
            //Scan the image for target barcode(s)
            if(r.read())
            {
                //barcode found.
                System.out.println("CodeText: " + r.getCodeText());
            }
            else
            {
                System.out.println("Not found.");
            }
            r.close();
        }
        catch (Exception ex) { 
            System.out.println("Exception occured.");
            ex.printStackTrace();
        }

    }
}
